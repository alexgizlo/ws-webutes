package com.utes.spring.web.app.restController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.utes.spring.web.app.dto.PersonaDTO;
import com.utes.spring.web.app.service.EstudianteService;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping(value = "/api/estudiante")
public class EstudiantesWSRestController {

	@Autowired
	private EstudianteService estudianteService;

	@RequestMapping(value = "/inscripcion/habilitaBotonInscripcion", method = RequestMethod.POST, consumes = {
			"application/json" }, produces = { "application/json" + ";charset=utf-8" })
	public @ResponseBody boolean habilitaBotonInscripcion(@RequestBody PersonaDTO persona) {
		return estudianteService.habilitaBotonInscripcion(persona);
	}

}
