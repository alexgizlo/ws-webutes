package com.utes.spring.web.app.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.utes.spring.web.app.dto.PersonaDTO;
import com.utes.spring.web.app.entity.Persona;

@Service
public interface PersonaService extends IParsable<PersonaDTO, Persona> {

	public PersonaDTO create(PersonaDTO obj);

	public PersonaDTO create2(PersonaDTO obj);

	public boolean update(PersonaDTO obj);

	public boolean delete(Integer id);

	public List<PersonaDTO> obtenerListadoPersona();

	public PersonaDTO obtenerPersonaPorId(Integer id, Object... b);

	public PersonaDTO obtenerPersonaPorCedula(String cedula);

}
