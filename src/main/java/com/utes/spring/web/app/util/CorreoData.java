package com.utes.spring.web.app.util;

import java.io.Serializable;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;

import com.utes.spring.web.app.dto.PersonaDTO;
import com.utes.spring.web.app.dto.SysUsuarioDTO;
import com.utes.spring.web.app.service.SysUsuarioService;

import lombok.Data;

@Data
public class CorreoData implements Serializable {

	private static final long serialVersionUID = 1L;

	Mail me;
	@Autowired
	private SysUsuarioService sysUsuario;
	ArrayList<Object> arraydata;
	private SysUsuarioDTO auxuser;
	private String param1, param2, param3, param4, param5;
	private String asuntos;

	public CorreoData() {
		me = new Mail();
		arraydata = new ArrayList();
		auxuser = new SysUsuarioDTO();
	}

	public void setData(PersonaDTO enper, String asunto, String introduccion, String detalle) {
		auxuser = this.sysUsuario.obtenerUsuarioPorPersona(enper);
		setAsuntos(asunto);
		setParam1(auxuser.getPersona().getPerApellido() + " " + auxuser.getPersona().getPerNombre());
		setParam2(introduccion);
		setParam3(detalle);
	}

	public void setDataUsuario(SysUsuarioDTO enusuario, String asunto, String introduccion, String detalle) {
		this.auxuser = this.sysUsuario.obtenerUsuarioPorId(enusuario.getIdUsr());
		setAsuntos(asunto);
		setParam1(auxuser.getPersona().getPerApellido() + " " + auxuser.getPersona().getPerNombre());
		setParam2(introduccion);
		setParam3(detalle);
	}

	public void sendNotificaNuevo() {
		try {
			me.configureDataServerMail();
			arraydata.clear();
			arraydata.add(getParam1());
			arraydata.add(getParam2());
			arraydata.add(getParam3());
			arraydata.add("");
			arraydata.add("");

			me.configureDataParams(arraydata, this.auxuser.getUsrCorreo(), getAsuntos());

			me.ejecutaProcesoBusqueda();
			setAsuntos("");
			setParam1("");
			setParam2("");
			setParam3("");
		} catch (Exception e) {
			// log.error(e);
		}

	}

}
