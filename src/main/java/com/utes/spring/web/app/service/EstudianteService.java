package com.utes.spring.web.app.service;

import org.springframework.stereotype.Service;

import com.utes.spring.web.app.dto.PersonaDTO;

@Service
public interface EstudianteService {

	public boolean habilitaBotonInscripcion(PersonaDTO enpersonasesion);
}
