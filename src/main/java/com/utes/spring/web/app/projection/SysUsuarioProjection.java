package com.utes.spring.web.app.projection;

import java.util.Date;

public interface SysUsuarioProjection {

	Integer getIdUsr();
	Integer getIdPersona();
	Integer getIdSysPerfil();
	String getUsrUsuario();
	String getUsrClave();
	String getUsrCorreo();
	Boolean getUsrActivo();
	Date getUsrFechaCreado();
	Date getUsrFechaEditado();
	
}
