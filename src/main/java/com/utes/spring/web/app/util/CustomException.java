package com.utes.spring.web.app.util;

import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestClientException;

//import com.conecel.claro.bnas.dxp.common.util.UsefulFunctions;
import com.google.common.base.Strings;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class CustomException extends Exception {

	private static final long serialVersionUID = 1L;

	private int errorCode = 500;
	private String exceptionMessage;
	private Object[] params;
	private String logCode;
	boolean messageProperty;
	HttpStatus httpStatus;

	public static CustomException generateResourceAccessException(String logCode, ResourceAccessException ex,
			Object... params) {
		CustomException customException = new CustomException();
		String exMessage = null;
		customException.setMessageProperty(!Strings.isNullOrEmpty(logCode));
		customException.setLogCode(logCode);
		if (!Strings.isNullOrEmpty(exMessage)) {
			customException.setExceptionMessage(exMessage);
		}
		customException.setParams(params);
		return customException;
	}

	public static CustomException generateHttpClientErrorException(String logCode, HttpClientErrorException ex,
			Object... params) {
		CustomException customException = new CustomException();
		String exMessage = null;
		if (ex != null)
			exMessage = UsefulFunctions.processRequestOracleError(ex.getResponseBodyAsString());
		customException.setMessageProperty(!Strings.isNullOrEmpty(logCode));
		customException.setLogCode(logCode);
		if (!Strings.isNullOrEmpty(exMessage)) {
			customException.setExceptionMessage(exMessage);
		}
		customException.setParams(params);
		return customException;
	}

	public static CustomException generateHttpClientErrorException(String logCode, HttpClientErrorException ex,
			boolean property, Object... params) {
		CustomException customException = new CustomException();
		String exMessage = null;
		if (ex != null)
			exMessage = UsefulFunctions.processRequestOracleError(ex.getResponseBodyAsString());
		customException.setMessageProperty(property);
		customException.setLogCode(logCode);
		if (!Strings.isNullOrEmpty(exMessage)) {
			customException.setExceptionMessage(exMessage);
		}
		customException.setParams(params);
		return customException;
	}

	public static CustomException generateHttpServerErrorException(String logCode, HttpServerErrorException ex,
			Object... params) {
		CustomException customException = new CustomException();
		String exMessage = null;
		if (ex != null)
			exMessage = UsefulFunctions.processRequestOracleError(ex.getResponseBodyAsString());
		if (exMessage == null || exMessage.isEmpty()) {
			exMessage = ex.getResponseBodyAsString();
		}
		customException.setMessageProperty(!Strings.isNullOrEmpty(logCode));
		customException.setLogCode(logCode);
		if (!Strings.isNullOrEmpty(exMessage)) {
			customException.setExceptionMessage(exMessage);
		}
		customException.setParams(params);
		return customException;
	}

	public static CustomException generateRestClientException(String logCode, RestClientException ex,
			Object... params) {
		CustomException customException = new CustomException();
		customException.setLogCode(logCode);
		customException.setMessageProperty(true);
		return customException;
	}

	public CustomException(String message) {
		super(message);
	}

	public CustomException(String exceptionMessage, String logMessage, Object... params) {
		super(exceptionMessage);
	}

	public CustomException(String exceptionMessage, String logMessage) {
		super(exceptionMessage);
	}

	public CustomException(int errorCode, String message) {
		super(message);
		this.errorCode = errorCode;
	}

	public CustomException(HttpStatus httpStatus, String message) {
		super(message);
		this.httpStatus = httpStatus;
	}

	public CustomException(Throwable throwable) {
		super(throwable);
	}

	public CustomException(String message, Throwable throwable) {
		super(message, throwable);
	}

}
