package com.utes.spring.web.app.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.utes.spring.web.app.EstaticosConfig;
import com.utes.spring.web.app.dto.AsignadoDTO;
import com.utes.spring.web.app.dto.DocenteReporteDTO;
import com.utes.spring.web.app.dto.GeneralDTO;
import com.utes.spring.web.app.dto.PeriodoDTO;
import com.utes.spring.web.app.dto.ResolucionDTO;
import com.utes.spring.web.app.dto.SeleccionDTO;
import com.utes.spring.web.app.dto.SysUsuarioDTO;
import com.utes.spring.web.app.dto.TemaDTO;
import com.utes.spring.web.app.service.AsignadoService;
import com.utes.spring.web.app.service.PeriodoService;
import com.utes.spring.web.app.service.ResolucionService;
import com.utes.spring.web.app.service.SeleccionService;
import com.utes.spring.web.app.service.SysUsuarioService;
import com.utes.spring.web.app.service.TemaService;

@Service
public class ConsultaService {

	@Autowired
	private SysUsuarioService sysUsuarioService;
	@Autowired
	private TemaService temaService;
	@Autowired
	private ResolucionService resolucionService;
	@Autowired
	private AsignadoService asignadoService;
	@Autowired
	private PeriodoService periodoService;
	@Autowired
	private SeleccionService seleccionService;
	@Autowired
	private EstaticosConfig Estaticos;
	@Autowired
	private TipoService tipo;
	@Autowired
	private EstadoService estado;

	public List<GeneralDTO> consultaGeneral(Integer valorDocenteId, Integer idperiodo) {
		List<GeneralDTO> listGeneral = new ArrayList<>();
		List<TemaDTO> listTemaPorUsuario = new ArrayList<>();
		try {
			listGeneral.clear();
			List<SysUsuarioDTO> listUsuarios = null;

			listUsuarios = sysUsuarioService
					.obtenerListadoUsuarioPorTemaPorPerfil(Estaticos.getTIPO_LABEL_PERFIL_DOCENTE(), valorDocenteId,
							Estaticos.getESTADO_TEMA_POST_APROBADO() + ","
									+ Estaticos.getESTADO_TEMA_POST_LECTORPROYECTO() + ","
									+ Estaticos.getESTADO_TEMA_POST_CERRADO(),
							idperiodo);

			int ntemas = 0;
			int ntema0 = 0, ntema1 = 0, ntema2 = 0, ntema3 = 0, ntema4 = 0;
			double porcavance = 0.0;
			double sumaporavance = 0.0, promedio = 0.0;
			String avance = "", docentenombre = "";
			int nvigente = 0;

			if (listUsuarios != null) {
				for (SysUsuarioDTO objlineaUsuario : listUsuarios) {
					if (objlineaUsuario != null) {
						listTemaPorUsuario = temaService.obtenerTemasxUsuarioEstado(
								objlineaUsuario.getUsrUsuario(),
								Estaticos.getESTADO_TEMA_POST_APROBADO() + ","
										+ Estaticos.getESTADO_TEMA_POST_LECTORPROYECTO() + ","
										+ Estaticos.getESTADO_TEMA_POST_CERRADO(),
								idperiodo);

						for (TemaDTO objtema : listTemaPorUsuario) {
							if (objtema != null) {
								if (objtema.getTemPorcAvance() != null) {
									avance = "" + objtema.getTemPorcAvance();
								} else {
									avance = "0";
								}
								if (avance.isEmpty() != true && avance.equals("null") != true) {
									porcavance = Double.parseDouble(avance);
									if (porcavance == 0) {
										ntema0++;
									} else if (porcavance > 0 && porcavance <= 50) {
										ntema1++;
									} else if (porcavance > 50 && porcavance <= 70) {
										ntema2++;
									} else if (porcavance > 70 && porcavance < 90) {
										ntema3++;
									} else if (porcavance >= 90 && porcavance <= 100) {
										ntema4++;
									}
									sumaporavance += porcavance;
									ntemas++;
								}
							}
						}

						listTemaPorUsuario.clear();
						docentenombre = objlineaUsuario.getPersona().getPerApellido() + " "
								+ objlineaUsuario.getPersona().getPerNombre();

						if (idperiodo != null) {
							PeriodoDTO enperiodo = this.periodoService.obtenerPeriodoPorId(idperiodo);
							if (enperiodo != null) {
								SeleccionDTO ensel = this.seleccionService.obtenerSeleccionPorPeriodoPersona(
										enperiodo.getPrdNumero(), objlineaUsuario.getPersona().getIdPer());
								if (ensel != null) {

									if (ensel.getSelHoraVigente() == 0 || ensel.getSelHoraVigente() < 0) {

									} else {
										nvigente = ensel.getSelHoraVigente();
									}
								}
							}
						} else {
							// Mensajes.mensajeWarn(null, "Es necesario seleccionar un periodo", null);
						}

					}

					if (sumaporavance != 0 && ntemas != 0) {
						promedio = ((sumaporavance / ntemas) * 1000 / 1000);
					}

					listGeneral.add(new GeneralDTO(docentenombre, nvigente, RedondeaNumero(promedio, 2), ntema0, ntema1,
							ntema2, ntema3, ntema4, porcavance, ntemas));
					promedio = 0.0;
					sumaporavance = 0.0;
					ntemas = 0;
					ntema0 = 0;
					ntema1 = 0;
					ntema2 = 0;
					ntema3 = 0;
					ntema4 = 0;

				}
			}

		} catch (NumberFormatException e) {
			// log.error(e);
		}
		return listGeneral;
	}

	public List<DocenteReporteDTO> consultaDocente(Integer valorDocenteId, Integer idperiodo, Date fresini,
			Date fresfin, Date finiini, Date finifin) {
		List<DocenteReporteDTO> listDocenteReporte = new ArrayList<>();
		List<TemaDTO> listTemaGeneral = new ArrayList<>();
		try {
			listDocenteReporte.clear();
			List<SysUsuarioDTO> listUsuarios = null;

			listUsuarios = sysUsuarioService.obtenerListadoUsuarioPorTemaPorPerfil(
					Estaticos.getTIPO_LABEL_PERFIL_DOCENTE(), valorDocenteId,
					Estaticos.getESTADO_TEMA_POST_APROBADO() + "," + Estaticos.getESTADO_TEMA_POST_LECTORPROYECTO()
							+ "," + Estaticos.getESTADO_TEMA_POST_CERRADO() + ","
							+ Estaticos.getESTADO_TEMA_POST_PRORROGA());

			int ntema0 = 0;
			String fechaini = "", fechafin = "", fechaaprob = "", fecharesol = "", numres = "", nom1 = "", nom2 = "";

			int ccuenta = 0;
			int contdataos = 0;
			if (listUsuarios != null) {
				for (SysUsuarioDTO objlinea : listUsuarios) {
					if (objlinea != null) {
						listTemaGeneral = temaService.obtenerTemasxUsuarioEstadoConvocatoria(objlinea.getUsrUsuario(),
								Estaticos.getESTADO_TEMA_POST_APROBADO() + ","
										+ Estaticos.getESTADO_TEMA_POST_LECTORPROYECTO() + ","
										+ Estaticos.getESTADO_TEMA_POST_CERRADO() + ","
										+ Estaticos.getESTADO_TEMA_POST_PRORROGA(),
								idperiodo, fresini, fresfin, finiini, finifin);

						for (TemaDTO objtema : listTemaGeneral) {
							if (objtema != null) {
								List<ResolucionDTO> listresaux = resolucionService
										.obtenerResolucionxTema(objtema.getIdTem());
								for (ResolucionDTO resolucion : listresaux) {
									if (resolucion != null) {
										if (resolucion.getTipoResolucion().getIdTrsl() == Estaticos
												.getESTADO_TEMA_POST_PRORROGA()) {
											ntema0++;
											fechaaprob = resolucion.getRslFechaResolucion() + " ";
											fechafin = resolucion.getRslFechaEntrega() + " ";
											fechaini = resolucion.getRslFechaInicio() + " ";
											fecharesol = resolucion.getRslFechaResolucion() + " ";
											numres = resolucion.getRslNumero() + " ";

										}
										if (resolucion.getTipoResolucion().getIdTrsl() == Estaticos
												.getESTADO_TEMA_POST_APROBADO()) {
											fechaaprob = resolucion.getRslFechaResolucion() + " ";
											fechafin = resolucion.getRslFechaEntrega() + " ";
											fechaini = resolucion.getRslFechaInicio() + " ";
											fecharesol = resolucion.getRslFechaResolucion() + " ";
											numres = resolucion.getRslNumero() + " ";
										}
									} else {
										fechaaprob = "S/N";
										fechafin = "S/N";
										fechaini = "S/N";
										fecharesol = "S/N";
										numres = "S/N";
									}
								}
								List<AsignadoDTO> listasig = asignadoService.obtenerAsignadoxIdsTipos(
										"" + objtema.getIdTem(), "" + Estaticos.getTIPO_ID_ASIGNACION_ESTUDIANTE());
								for (AsignadoDTO asignado : listasig) {
									if (asignado != null) {
										if (ccuenta == 0) {
											nom1 = asignado.getPersona().getPerApellido() + " "
													+ asignado.getPersona().getPerNombre();
										} else {
											nom2 = asignado.getPersona().getPerApellido() + " "
													+ asignado.getPersona().getPerNombre();
										}
										ccuenta++;
									}
								}
								String valoravance = "";
								if (objtema.getTemPorcAvance() != null
										&& objtema.getTemPorcAvance().equals("null") != true) {

									valoravance = "" + objtema.getTemPorcAvance();
								} else {
									valoravance = "0";
								}
								StringBuilder autor = new StringBuilder();
								autor.append(objtema.getPersona().getPerApellido());
								autor.append(" ");
								autor.append(objtema.getPersona().getPerNombre());
								listDocenteReporte.add(new DocenteReporteDTO(objtema.getTemNombre(), valoravance,
										"" + ntema0, fechaini, nom1, nom2, numres, fechaini, fechafin,
										tipo.getNombreTipoPorLista(objtema.getTemIdTipo(), tipo.loadDocumentoAll()),
										autor.toString(), objtema.getTemIdPeriodo(), fecharesol));
								contdataos++;
								fechaaprob = " ";
								fechafin = " ";
								fechaini = " ";
								numres = " ";
								nom1 = "";
								nom2 = "";
								ccuenta = 0;
							}

						}
					}
				}
			}
		} catch (NumberFormatException e) {
			// log.error(e);
		}
		return listDocenteReporte;
	}

	public List<DocenteReporteDTO> consultaDocenteEstado(Integer valorDocenteId, Integer idperiodo, Date fresini,
			Date fresfin, Date finiini, Date finifin) {
		List<DocenteReporteDTO> listDocenteReporte = new ArrayList<>();
		List<TemaDTO> listTemaGeneral = new ArrayList<>();
		String todo_Estados = null;
		try {
			todo_Estados = this.estado.getListaString(this.estado.loadInscripcionAccion());
			listDocenteReporte.clear();
			List<SysUsuarioDTO> listUsuarios = null;

			if (valorDocenteId != null) {
				listUsuarios = sysUsuarioService.obtenerListadoUsuarioPorTemaPorPerfil(
						Estaticos.getTIPO_LABEL_PERFIL_DOCENTE(), valorDocenteId, null);
			} else {
				listUsuarios = sysUsuarioService
						.obtenerListadoUsuarioPorTemaPorPerfil(Estaticos.getTIPO_LABEL_PERFIL_DOCENTE(), 0, null);
			}

			int ntema0 = 0;
			String fechaini = "", fechafin = "", fechaaprob = "", fecharesol = "", numres = "", nom1 = "", nom2 = "";

			int ccuenta = 0;
			int contdataos = 0;
			if (listUsuarios != null) {
				for (SysUsuarioDTO objlinea : listUsuarios) {
					if (objlinea != null) {
						listTemaGeneral = temaService.obtenerTemasxUsuarioEstadoConvocatoria(objlinea.getUsrUsuario(),
								Estaticos.getESTADO_TEMA_POST_APROBADO() + ","
										+ Estaticos.getESTADO_TEMA_POST_LECTORPROYECTO() + ","
										+ Estaticos.getESTADO_TEMA_POST_CERRADO() + ","
										+ Estaticos.getESTADO_TEMA_POST_PRORROGA(),
								idperiodo, fresini, fresfin, finiini, finifin);

						for (TemaDTO objtema : listTemaGeneral) {
							if (objtema != null) {
								List<ResolucionDTO> listresaux = resolucionService
										.obtenerResolucionxTema(objtema.getIdTem());
								for (ResolucionDTO resolucion : listresaux) {
									if (resolucion != null) {
										if (resolucion.getTipoResolucion().getIdTrsl() == Estaticos
												.getESTADO_TEMA_POST_PRORROGA()) {
											ntema0++;
											fechaaprob = resolucion.getRslFechaResolucion() + " ";
											fechafin = resolucion.getRslFechaEntrega() + " ";
											fechaini = resolucion.getRslFechaInicio() + " ";
											fecharesol = resolucion.getRslFechaResolucion() + " ";
											numres = resolucion.getRslNumero() + " ";

										}
										if (resolucion.getTipoResolucion().getIdTrsl() == Estaticos
												.getESTADO_TEMA_POST_APROBADO()) {
											fechaaprob = resolucion.getRslFechaResolucion() + " ";
											fechafin = resolucion.getRslFechaEntrega() + " ";
											fechaini = resolucion.getRslFechaInicio() + " ";
											fecharesol = resolucion.getRslFechaResolucion() + " ";
											numres = resolucion.getRslNumero() + " ";
										}
									} else {
										fechaaprob = "S/N";
										fechafin = "S/N";
										fechaini = "S/N";
										fecharesol = "S/N";
										numres = "S/N";
									}
								}
								List<AsignadoDTO> listasig = asignadoService.obtenerAsignadoxIdsTipos(
										"" + objtema.getIdTem(), "" + Estaticos.getTIPO_ID_ASIGNACION_ESTUDIANTE());
								for (AsignadoDTO asignado : listasig) {
									if (asignado != null) {
										if (ccuenta == 0) {
											nom1 = asignado.getPersona().getPerApellido() + " "
													+ asignado.getPersona().getPerNombre();
										} else {
											nom2 = asignado.getPersona().getPerApellido() + " "
													+ asignado.getPersona().getPerNombre();
										}
										ccuenta++;
									}
								}
								String valoravance = "";
								if (objtema.getTemPorcAvance() != null
										&& objtema.getTemPorcAvance().equals("null") != true) {

									valoravance = "" + objtema.getTemPorcAvance();
								} else {
									valoravance = "0";
								}
								StringBuilder autor = new StringBuilder();
								autor.append(objtema.getPersona().getPerApellido());
								autor.append(" ");
								autor.append(objtema.getPersona().getPerNombre());
								listDocenteReporte.add(new DocenteReporteDTO(objtema.getTemNombre(), valoravance,
										"" + ntema0, fechaini, nom1, nom2, numres, fechaini, fechafin,
										tipo.getNombreTipoPorLista(objtema.getTemIdTipo(), tipo.loadDocumentoAll()),
										autor.toString(), objtema.getTemIdPeriodo(), fecharesol));
								contdataos++;
								fechaaprob = " ";
								fechafin = " ";
								fechaini = " ";
								numres = " ";
								nom1 = "";
								nom2 = "";
								ccuenta = 0;
							}

						}
					}
				}
			}
		} catch (NumberFormatException e) {
			// log.error(e);
		}
		return listDocenteReporte;
	}

	public static double RedondeaNumero(double numero, int digitos) {
		int cifras = (int) Math.pow(10, digitos);
		return Math.rint(numero * cifras) / cifras;
	}
}
