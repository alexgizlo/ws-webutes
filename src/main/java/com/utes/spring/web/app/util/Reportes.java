package com.utes.spring.web.app.util;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.springframework.core.io.ClassPathResource;

import com.utes.spring.web.app.dto.PresolicitudDTO;
import com.utes.spring.web.app.dto.TemaDTO;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class Reportes {

	public void exportaInscripcion(List<PresolicitudDTO> data, Map<String, Object> parametros, String nombrejasper)
			throws JRException, IOException {

		File jasper = new ClassPathResource("/reportes/" + nombrejasper + ".jasper").getFile();
		// File jasper = new
		// File(FacesContext.getCurrentInstance().getExternalContext().getRealPath("/reportes/"
		// + nombrejasper + ".jasper"));
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasper.getPath(), parametros,
				new JRBeanCollectionDataSource(data));

		// HttpServletResponse response = (HttpServletResponse)
		// FacesContext.getCurrentInstance().getExternalContext().getResponse();
		// response.addHeader("Content-disposition", "attachment;
		// filename=rptInscripcion.pdf");
		// ServletOutputStream stream = response.getOutputStream();
		// JasperExportManager.exportReportToPdfStream(jasperPrint, stream);
		// stream.flush();
		// stream.close();
		// FacesContext.getCurrentInstance().responseComplete();
	}

	public JasperPrint exportaTema(List<TemaDTO> data, Map<String, Object> parametros, String nombrejasper)	{

		//String path = resourceLoader.getResource("classpath:rpt_users.jrxml").getURI().getPath();
		  		  
		File jasper;
		JasperPrint jasperPrint = null;
		try {
			jasper = new ClassPathResource("/reportes/" + nombrejasper + ".jrxml").getFile();

			JasperReport jasperReport = JasperCompileManager.compileReport(jasper.getPath());		
			jasperPrint = JasperFillManager.fillReport(jasperReport, parametros,
					new JRBeanCollectionDataSource(data));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jasperPrint;

	}
}
