package com.utes.spring.web.app.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class AsignaEstudianteDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private TemaDTO enTemaDetalleSeleccion;
	private Integer idpersona;
	private SysUsuarioDTO enusuariosesion;
}
