package com.utes.spring.web.app.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.swing.SwingWorker;

import org.springframework.beans.factory.annotation.Autowired;

import com.utes.spring.web.app.dto.CorreoDTO;
import com.utes.spring.web.app.service.CorreoService;

import lombok.Data;

@Data
public class Mail implements Serializable {

	private static final long serialVersionUID = 1L;
	// static Logger log = Logger.getLogger(Mail.class);

	private String host;
	private String protocolo;
	private String usuario;
	private String pass;
	private String puerto;
	private String auth;
	private String html;
	private ArrayList<Object> params;
	private String correo;
	private String asunto;
	private boolean enviando;

	@Autowired
	private CorreoService correoService;

	public Mail() {

	}

	public void configureDataServerMail() {
		CorreoDTO encorreo = this.correoService.obtenerCorreo(true).get(0);
		setHost(encorreo.getCorreoHost());
		setProtocolo(encorreo.getCorreoProtocolo());
		setUsuario(encorreo.getCorreoUsuario());
		setPass(encorreo.getCorreoClave());
		setPuerto(encorreo.getCorreoPuerto());
		setAuth(encorreo.getCorreoAuth().toString().toUpperCase());
		setHtml(encorreo.getCorreoNotificacion());
	}

	private Properties configureServelMail() {
		Properties props = System.getProperties();
		props.put("mail.transport.protocol", getProtocolo());
		props.put("mail.smtps.auth", getAuth());
		props.put("mail.smtps.port", getPuerto());
		props.put("mail.smtps.ssl.trust", getHost());
		return props;
	}

	public void configureDataParams(ArrayList<Object> params, String correo, String asunto) {
		setCorreo(correo);
		setAsunto(asunto);
		setParams(params);
	}

	public String setParamData(String html, ArrayList<Object> params) {
		String cambio, aux1, aux2, aux3, aux4, aux5;
		cambio = html;
		aux1 = cambio.replace("param1", params.get(0).toString());
		aux2 = aux1.replace("param2", params.get(1).toString());
		aux3 = aux2.replace("param3", params.get(2).toString());
		aux4 = aux3.replace("param4", params.get(3).toString());
		aux5 = aux4.replace("param5", params.get(4).toString());
		return aux5;
	}

	public void ejecutaProcesoBusqueda() {
		final SwingWorker worker = new SwingWorker() {
			@Override
			protected Object doInBackground() throws Exception {
				boolean sessionDebug = false;
				String docs[] = { "C:\\Eclinic\\reporte.pdf", "C:\\Eclinic\\cita.pdf" };
				Session mailSession = Session.getDefaultInstance(configureServelMail(), null);
				mailSession.setDebug(sessionDebug);
				boolean enviado = sendMessage(getCorreo(), setParamData(getHtml(), getParams()), getAsunto(), false,
						docs, mailSession);
				setEnviando(enviado);
				return null;
			}

			@Override
			protected void done() {
				/**
				 * txta_ver.append(new DATE().dateValue()+"" + modelo.getDatatext());
				 */
			}
		};

		worker.execute();
	}

	private boolean sendMessage(String maildestinatario, String htmlcontent, String mailasunto, boolean adddoc,
			String[] docs, Session mailSession) {
		boolean envio;

		try {
			Message msg = new MimeMessage(mailSession);
			msg.setFrom(new InternetAddress(getUsuario()));
			InternetAddress[] address = { new InternetAddress(maildestinatario) };
			msg.setRecipients(Message.RecipientType.TO, address);
			msg.setSubject(mailasunto);
			Multipart multipart = new MimeMultipart();

			BodyPart messageBodyPart = new MimeBodyPart();

			messageBodyPart.setContent(htmlcontent, "text/html");
			multipart.addBodyPart(messageBodyPart);

			msg.setContent(multipart);

			Transport transport = mailSession.getTransport(getProtocolo());

			transport.connect(getHost(), getUsuario(), getPass());
			transport.sendMessage(msg, msg.getAllRecipients());
			envio = true;
			transport.close();
		} catch (javax.mail.MessagingException e) {
			envio = false;
			// log.error(e);
		}

		return envio;
	}

	
}
