package com.utes.spring.web.app.restController;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
public class ConsultaDocenteDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	private Integer idUsr;
	private String estados;
	private Integer idPrd;
	private Date temFechaResolucionIni;
	private Date temFechaResolucionFin;
	private Date temFechaInicio;
	private Date temFechaInicioFin;
	private Date temFechaEntrega;
	private Date temFechaEntregaFin;
}
