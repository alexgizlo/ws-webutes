package com.utes.spring.web.app.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.utes.spring.web.app.EstaticosConfig;
import com.utes.spring.web.app.dto.AsignadoDTO;
import com.utes.spring.web.app.dto.InscripcionDTO;
import com.utes.spring.web.app.dto.PeriodoDTO;
import com.utes.spring.web.app.dto.PersonaDTO;
import com.utes.spring.web.app.dto.PresolicitudDTO;
import com.utes.spring.web.app.dto.SysUsuarioDTO;
import com.utes.spring.web.app.dto.TemaDTO;
import com.utes.spring.web.app.service.AsignadoService;
import com.utes.spring.web.app.service.DocenteService;
import com.utes.spring.web.app.service.InscripcionService;
import com.utes.spring.web.app.service.PeriodoService;
import com.utes.spring.web.app.service.PersonaService;
import com.utes.spring.web.app.service.PresolicitudService;
import com.utes.spring.web.app.service.SysConfiguracionService;
import com.utes.spring.web.app.service.SysUsuarioService;
import com.utes.spring.web.app.service.TemaService;
import com.utes.spring.web.app.util.CorreoData;
import com.utes.spring.web.app.util.CustomException;

@Service("DocenteService")
public class DocenteServiceImp implements DocenteService {

	@Autowired
	private AsignadoService asignadoService;
	@Autowired
	private TemaService temaService;
	@Autowired
	private SysConfiguracionService sysConfiguracionService;
	@Autowired
	private EstaticosConfig Estaticos;
	@Autowired
	private SysUsuarioService sysUsuarioService;
	@Autowired
	private PeriodoService periodoService;
	@Autowired
	private InscripcionService inscripcionService;
	@Autowired
	private PersonaService personaService;
	@Autowired
	private PresolicitudService presolicitudService;
	@Autowired
	private EstadoService estadoService;
	private CorreoData utilcorreo;

	@Override
	public boolean asignaEstudianteTema(TemaDTO enTemaDetalleSeleccion, Integer idpersona,
			SysUsuarioDTO enusuariosesion) throws CustomException {
		try {
			if (enTemaDetalleSeleccion != null) {
				List<AsignadoDTO> listAsignadoTema = this.asignadoService.obtenerAsignadoxTema(
						enTemaDetalleSeleccion.getIdTem(), Estaticos.getTIPO_ID_ASIGNACION_ESTUDIANTE());
				Date utilfecha = new Date();
				if ((listAsignadoTema.size() + 1) <= enTemaDetalleSeleccion.getTemNumEst()) {
					AsignadoDTO enasignado = new AsignadoDTO();
					enasignado.setAsgActivo(true);
					enasignado.setIdPersona(idpersona);
					enasignado.setIdTema(enTemaDetalleSeleccion.getIdTem());
					enasignado.setAsgFechaRegistro(utilfecha);
					enasignado.setAsgIdTipo(Estaticos.getTIPO_ID_ASIGNACION_ESTUDIANTE());
					enasignado.setAsgIdEstadoTema(Estaticos.getESTADO_TEMA_PRE_ASIGNA_ESTUDIANTE());

					if (this.asignadoService.create(enasignado)) {
						enTemaDetalleSeleccion.setTemIdEstado(Estaticos.getESTADO_TEMA_PRE_ASIGNAESTUDIANTE());

						if (this.temaService.update(enTemaDetalleSeleccion)) {

							/**
							 * envio a docente osea usaurio sesion
							 */

							if (this.sysConfiguracionService
									.activaProcesoByCampo(Estaticos.getCONFIG_ASIGNA_TEMAESTUDIANTE())) {
								utilcorreo.setDataUsuario(enusuariosesion, "Asignacción tema",
										"Se ha asignado al tema " + enTemaDetalleSeleccion.getTemNombre(),
										"Estudiante " + enasignado.getPersona().getPerApellido() + " "
												+ enasignado.getPersona().getPerNombre());
								// utilcorreo.sendNotificaNuevo();

								/**
								 * ENVIO A DOCENTE
								 */
								SysUsuarioDTO auxuser = this.sysUsuarioService
										.obtenerUsuarioPorPersonaId(enasignado.getPersona().getIdPer());

								utilcorreo.setDataUsuario(auxuser, "Asignacción tema",
										"Se ha asignado al tema " + enTemaDetalleSeleccion.getTemNombre(),
										"Estudiante " + enasignado.getPersona().getPerApellido() + " "
												+ enasignado.getPersona().getPerNombre());
								// utilcorreo.sendNotificaNuevo();
								// Mensajes.mensajeInfo(null, MENSAJE_OK_REGISTRA, null);
							}
						} else {
							// Mensajes.mensajeError(null, "TEMA " + MENSAJE_ERROR_ACTUALIZA, null);
							throw new CustomException(HttpStatus.CONFLICT, "MENSAJE_ERROR_ACTUALIZA");
						}
					} else {
						throw new CustomException(HttpStatus.CONFLICT, "MENSAJE_ERROR_ACTUALIZA");
					}
				} else {
					throw new CustomException(HttpStatus.CONFLICT, "MENSAJE_ERROR_ACTUALIZA");
				}

			}

		} catch (Exception e) {
			throw new CustomException(HttpStatus.CONFLICT, "MENSAJE_ERROR_ACTUALIZA" + e.toString());
		}
		return true;
	}

	@Override
	public String getEstadobyInscripcion(String perced) {
		String cadenaestado = "";
		try {
			Integer idlastperiodo = this.periodoService.obtenerUltimoRegistroPeriodo();
			if (idlastperiodo != null) {
				PeriodoDTO enperiodoActual = this.periodoService.obtenerPeriodoPorIdActivo(idlastperiodo, true);
				if (enperiodoActual != null) {
					InscripcionDTO eninscripionActual = this.inscripcionService
							.obtenerInscripcionPorPeriodo(enperiodoActual.getIdPrd());
					PersonaDTO auxpersonaActual = this.personaService.obtenerPersonaPorCedula(perced);
					PresolicitudDTO enpresl = this.presolicitudService.obtenerPresolicitudPorPersonaIdInscripcion(
							auxpersonaActual.getIdPer(), eninscripionActual.getIdIns());
					if (enpresl != null) {
						cadenaestado = this.estadoService.getNombreEstadoPorLista(enpresl.getPslIdEstado(),
								this.estadoService.loadInscripcion());
					} else {
						cadenaestado = "NO DEFINIDO";
					}
				}
			} else {
				// Mensajes.mensajeError(null, MENSAJE_ERROR_NOPERIODO, evtarea);
			}

		} catch (NumberFormatException e) {
			// log.error(e);
		}
		return cadenaestado;
	}

}
