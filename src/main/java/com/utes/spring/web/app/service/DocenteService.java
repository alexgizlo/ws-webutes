package com.utes.spring.web.app.service;

import org.springframework.stereotype.Service;

import com.utes.spring.web.app.dto.SysUsuarioDTO;
import com.utes.spring.web.app.dto.TemaDTO;
import com.utes.spring.web.app.util.CustomException;

@Service
public interface DocenteService {

	public boolean asignaEstudianteTema(TemaDTO enTemaDetalleSeleccion, Integer idpersona,
			SysUsuarioDTO enusuariosesion) throws CustomException;

	public String getEstadobyInscripcion(String percedula);
}
