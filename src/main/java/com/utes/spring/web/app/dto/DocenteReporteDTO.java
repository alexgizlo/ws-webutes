package com.utes.spring.web.app.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class DocenteReporteDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String nombreTema;
	private String porcAvanceTema;
	private String contadorProrroga;
	private String fechaInicio;
	private String nombreAutor1;
	private String nombreAutor2;
	private String noResolucion;
	private String fechaAprobacion;
	private String fechaEntrega;
	private String tipoProyecto;
	private String nombreTutor;
	private Integer periodoAprobado;
	private String fechaResolucion;
	private String estadoProyecto;

	public DocenteReporteDTO(String nombreTema, String porcAvanceTema, String contadorProrroga, String fechaInicio,
	          String nombreAutor1, String nombreAutor2, String noResolucion, String fechaAprobacion,
	          String fechaEntrega, String tipoProyecto, String nombreTutor, Integer periodoAprobado, String fechaResolucion) {
	    this.nombreTema = nombreTema;
	    this.porcAvanceTema = porcAvanceTema;
	    this.contadorProrroga = contadorProrroga;
	    this.fechaInicio = fechaInicio;
	    this.nombreAutor1 = nombreAutor1;
	    this.nombreAutor2 = nombreAutor2;
	    this.noResolucion = noResolucion;
	    this.fechaAprobacion = fechaAprobacion;
	    this.fechaEntrega = fechaEntrega;
	    this.tipoProyecto = tipoProyecto;
	    this.nombreTutor = nombreTutor;
	    this.periodoAprobado = periodoAprobado;
	    this.fechaResolucion = fechaResolucion;
	  }

}
