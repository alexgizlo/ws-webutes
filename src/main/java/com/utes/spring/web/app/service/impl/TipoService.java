package com.utes.spring.web.app.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.utes.spring.web.app.EstaticosConfig;
import com.utes.spring.web.app.dto.TipoDTO;

@Service
public class TipoService {

	@Autowired
	private EstaticosConfig estaticos;
	
	public List<TipoDTO> loadDocumentoAll() {
        return new ArrayList<TipoDTO>() {{
            add(new TipoDTO(estaticos.getTIPO_ID_OPCIONTITULACION_PROYECTO(), estaticos.getTIPO_LABEL_TITULACION_PROYECTO()));
            add(new TipoDTO(estaticos.getTIPO_ID_OPCIONTITULACION_ARTICULO(), estaticos.getTIPO_LABEL_TITULACION_ARTICULO()));
            add(new TipoDTO(estaticos.getTIPO_ID_OPCIONTITULACION_EXAMEN(), estaticos.getTIPO_LABEL_TITULACION_EXAMEN()));
        }};
    }
	
    public List<TipoDTO> loadDocumento() {
        return new ArrayList<TipoDTO>() {{
            add(new TipoDTO(estaticos.getTIPO_ID_OPCIONTITULACION_PROYECTO(), estaticos.getTIPO_LABEL_TITULACION_PROYECTO()));
            add(new TipoDTO(estaticos.getTIPO_ID_OPCIONTITULACION_ARTICULO(), estaticos.getTIPO_LABEL_TITULACION_ARTICULO()));
        }};
    }

    public List<TipoDTO> getListTipoTema() {
        return new ArrayList<TipoDTO>() {{
            add(new TipoDTO(estaticos.getTIPO_ID_OPCIONTITULACION_PROYECTO(), estaticos.getTIPO_LABEL_TITULACION_PROYECTO()));
            add(new TipoDTO(estaticos.getTIPO_ID_OPCIONTITULACION_ARTICULO(), estaticos.getTIPO_LABEL_TITULACION_ARTICULO()));
        }};
    }

    public List<TipoDTO> loadCuestionario() {
        return new ArrayList<TipoDTO>() {{
            add(new TipoDTO(estaticos.getTIPO_ID_CUESTIONARIO_INSCRIPCION(), estaticos.getTIPO_LABEL_CUESTIONARIO_INSCRIPCION()));
            add(new TipoDTO(estaticos.getTIPO_ID_CUESTIONARIO_PREREVISION(), estaticos.getTIPO_LABEL_CUESTIONARIO_PREREVISION()));   
        }};
    }    

    public List<TipoDTO> loadAsignacion() {
        return new ArrayList<TipoDTO>() {{
            add(new TipoDTO(estaticos.getTIPO_ID_ASIGNACION_REVISOR(), estaticos.getTIPO_LABEL_ASIGNACION_REVISOR()));
            add(new TipoDTO(estaticos.getTIPO_ID_ASIGNACION_ESTUDIANTE(), estaticos.getTIPO_LABEL_ASIGNACION_ESTUDIANTE()));
            add(new TipoDTO(estaticos.getTIPO_ID_ASIGNACION_LECTORPLAN(), estaticos.getTIPO_LABEL_ASIGNACION_LECTORPLAN()));
            add(new TipoDTO(estaticos.getTIPO_ID_ASIGNACION_LECTORPROYECTO(), estaticos.getTIPO_LABEL_ASIGNACION_LECTORPROYECTO()));
        }};
    }
    
	public String getNombreTipoPorLista(int id, List<TipoDTO> listatipos) {
		String data = "";
		for (TipoDTO estado : listatipos) {
			if (estado.getId() == id) {
				data = estado.getNombre();
				break;
			}
		}
		return data;
	}
	
}
