package com.utes.spring.web.app.service.impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import com.utes.spring.web.app.EstaticosConfig;
import com.utes.spring.web.app.dto.AsignadoDTO;
import com.utes.spring.web.app.dto.EvolucionDTO;
import com.utes.spring.web.app.dto.HitoDTO;
import com.utes.spring.web.app.dto.PresolicitudDTO;
import com.utes.spring.web.app.dto.SysUsuarioDTO;
import com.utes.spring.web.app.dto.TemaDTO;
import com.utes.spring.web.app.service.AsignadoService;
import com.utes.spring.web.app.service.PresolicitudService;
import com.utes.spring.web.app.service.SysUsuarioService;
import com.utes.spring.web.app.service.TemaService;
import com.utes.spring.web.app.util.Reportes;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Service
public class ReporteService {

	@Autowired
	private EstaticosConfig Estaticos;
	@Autowired
	private SysUsuarioService sysUsuarioService;
	@Autowired
	private AsignadoService asignadoService;
	@Autowired
	private TemaService temaService;
	@Autowired
	private PresolicitudService presolicitudService;
	File logo;
	//@Value("classpath:resources/img/logo-ups-home2.png")
	//Resource resourceFile;
	Reportes rpt = new Reportes();

	public ReporteService() {
		super();
		try {
			logo = new ClassPathResource("/img/logo-ups-home2.png").getFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public JasperPrint reporteInscripcionEstudiante(Integer idprsl, String usuario) {
		JasperPrint jasperPrint = null;
		Map<String, Object> param = new HashMap();
		
		List<PresolicitudDTO> listauxprsl = new ArrayList<PresolicitudDTO>();
		listauxprsl.add(presolicitudService.obtenerListadoPresolicitudPorId(idprsl));				
		
		param.put("usuario", usuario);
		param.put("estado", "");
		param.put("opcion", "");
		param.put("perfil", "");
		param.put("logo", logo.getPath());

		File jasper;
		try {
			jasper = new ClassPathResource("/reportes/rptPresolicitudEstudiante.jrxml").getFile();
			JasperReport jasperReport = JasperCompileManager.compileReport(jasper.getPath());		
			jasperPrint = JasperFillManager.fillReport(jasperReport, param,
					new JRBeanCollectionDataSource(listauxprsl));
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JRException e) {
			e.printStackTrace();
		}		
		return jasperPrint;
	}
	
	public JasperPrint reporteInscripcionListado(String usuario, List<PresolicitudDTO> listado) {
		JasperPrint jasperPrint = null;
		Map<String, Object> param = new HashMap();
				
		param.put("usuario", usuario);
		param.put("estado", "");
		param.put("opcion", "");
		param.put("perfil", "");
		param.put("logo", logo.getPath());

		File jasper;
		try {
			jasper = new ClassPathResource("/reportes/rptPresolicitudListado.jrxml").getFile();
			JasperReport jasperReport = JasperCompileManager.compileReport(jasper.getPath());		
			jasperPrint = JasperFillManager.fillReport(jasperReport, param,
					new JRBeanCollectionDataSource(listado));
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JRException e) {
			e.printStackTrace();
		}		
		return jasperPrint;
	}
	
	public JasperPrint reporteHito(Integer idPersona, Integer idTema) {
		JasperPrint jasperPrint = null;
		Map<String, Object> param = new HashMap();
		String estudiantes = "";
		int cont = 0;
		SysUsuarioDTO usuario = this.sysUsuarioService.obtenerUsuarioPorPersonaId(idPersona);
		String perfil = usuario.getSysPerfil().getPrfNombre();
		
		List<AsignadoDTO> listestudiantes = asignadoService.obtenerAsignadoxTema(idTema,
				Estaticos.getTIPO_ID_ASIGNACION_ESTUDIANTE());
		TemaDTO auxTema = temaService.obtenerTemasxId(idTema);
		List<HitoDTO> listado = auxTema.getHitos();
		
		for (AsignadoDTO obj : listestudiantes) {
			cont++;
			estudiantes += (obj.getPersona().getPerApellido().concat(" ").concat(obj.getPersona().getPerNombre()));
			if (listestudiantes.size() < cont) {
				estudiantes += ", ";
			}
		}			
		
		param.put("perfil", perfil);
		param.put("perfilestudiante", Estaticos.getTIPO_LABEL_PERFIL_ESTUDIANTE());
		param.put("estudiantes", estudiantes);
		param.put("usuario", usuario.getPersona().getPerApellido() + " " + usuario.getPersona().getPerNombre());
		param.put("estado", "");
		param.put("opcion", "");
		param.put("logo", logo.getPath());
		
		File jasper;
		try {
			jasper = new ClassPathResource("/reportes/rptHitoEstudiante.jrxml").getFile();
			JasperReport jasperReport = JasperCompileManager.compileReport(jasper.getPath());		
			jasperPrint = JasperFillManager.fillReport(jasperReport, param,
					new JRBeanCollectionDataSource(listado));
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JRException e) {
			e.printStackTrace();
		}		
		return jasperPrint;
	}
	
	public JasperPrint reporteEvolucion(Integer idPersona, Integer idTema) {
		JasperPrint jasperPrint = null;
		Map<String, Object> param = new HashMap();
		String estudiantes = "";
		int cont = 0;
		SysUsuarioDTO usuario = this.sysUsuarioService.obtenerUsuarioPorPersonaId(idPersona);
		String perfil = usuario.getSysPerfil().getPrfNombre();
		
		List<AsignadoDTO> listestudiantes = asignadoService.obtenerAsignadoxTema(idTema,
				Estaticos.getTIPO_ID_ASIGNACION_ESTUDIANTE());
		TemaDTO auxTema = temaService.obtenerTemasxId(idTema);
		List<EvolucionDTO> listado = auxTema.getEvoluciones(); 
		
		for (AsignadoDTO obj : listestudiantes) {
			cont++;
			estudiantes += (obj.getPersona().getPerApellido().concat(" ").concat(obj.getPersona().getPerNombre()));
			if (listestudiantes.size() < cont) {
				estudiantes += ", ";
			}
		}			
		
		param.put("perfil", perfil);
		param.put("perfilestudiante", Estaticos.getTIPO_LABEL_PERFIL_ESTUDIANTE());
		param.put("estudiantes", estudiantes);
		param.put("usuario", usuario.getPersona().getPerApellido() + " " + usuario.getPersona().getPerNombre());
		param.put("estado", "");
		param.put("opcion", "");
		param.put("logo", logo.getPath());
		
		File jasper;
		try {
			jasper = new ClassPathResource("/reportes/rptEvolucionEstudiante.jrxml").getFile();
			JasperReport jasperReport = JasperCompileManager.compileReport(jasper.getPath());		
			jasperPrint = JasperFillManager.fillReport(jasperReport, param,
					new JRBeanCollectionDataSource(listado));
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JRException e) {
			e.printStackTrace();
		}		
		return jasperPrint;
	}
	
	public JasperPrint reporteTema(Integer idPersona, Integer idTema) {
		JasperPrint jasperPrint = null;
		Map<String, Object> param = new HashMap();
		String estudiantes = "";
		int cont = 0;
		SysUsuarioDTO usuario = this.sysUsuarioService.obtenerUsuarioPorPersonaId(idPersona);
		String perfil = usuario.getSysPerfil().getPrfNombre();
		
		List<AsignadoDTO> listestudiantes = asignadoService.obtenerAsignadoxTema(idTema,
				Estaticos.getTIPO_ID_ASIGNACION_ESTUDIANTE());
		List<TemaDTO> auxlist = temaService.obtenerTemasxPk(idTema);
		
		for (AsignadoDTO obj : listestudiantes) {
			cont++;
			estudiantes += (obj.getPersona().getPerApellido().concat(" ").concat(obj.getPersona().getPerNombre()));
			if (listestudiantes.size() < cont) {
				estudiantes += ", ";
			}
		}			
		
		param.put("perfil", perfil);
		param.put("perfilestudiante", Estaticos.getTIPO_LABEL_PERFIL_ESTUDIANTE());
		param.put("estudiantes", estudiantes);
		param.put("usuario", usuario.getPersona().getPerApellido() + " " + usuario.getPersona().getPerNombre());
		param.put("estado", "");
		param.put("opcion", "");
		param.put("logo", logo.getPath());
		
		File jasper;
		try {
			jasper = new ClassPathResource("/reportes/rptTema.jrxml").getFile();
			JasperReport jasperReport = JasperCompileManager.compileReport(jasper.getPath());		
			jasperPrint = JasperFillManager.fillReport(jasperReport, param,
					new JRBeanCollectionDataSource(auxlist));
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JRException e) {
			e.printStackTrace();
		}		
		return jasperPrint;
	}
	
	
}
