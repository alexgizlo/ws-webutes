package com.utes.spring.web.app.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class GeneralDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private String docente;
	private Integer vigente;
	private Double promedioTema;
	private Integer contadorCero;
	private Integer contadorMenora;
	private Integer contadorEntrebc;
	private Integer contadorEntrecd;
	private Integer contadorEntrede;
	private Double porcentajeTema;
	private Integer contadorTema;

	public GeneralDTO(String docente, Integer vigente, Double promedioTema, Integer contadorCero,
			Integer contadorMenora, Integer contadorEntrebc, Integer contadorEntrecd, Integer contadorEntrede,
			Double porcentajeTema, Integer contadorTema) {
		this.docente = docente;
		this.vigente = vigente;
		this.promedioTema = promedioTema;
		this.contadorCero = contadorCero;
		this.contadorMenora = contadorMenora;
		this.contadorEntrebc = contadorEntrebc;
		this.contadorEntrecd = contadorEntrecd;
		this.contadorEntrede = contadorEntrede;
		this.porcentajeTema = porcentajeTema;
		this.contadorTema = contadorTema;
	}

}
