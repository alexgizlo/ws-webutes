package com.utes.spring.web.app.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.utes.spring.web.app.dto.EstadoDTO;
import com.utes.spring.web.app.dto.InscripcionDTO;
import com.utes.spring.web.app.dto.PersonaDTO;
import com.utes.spring.web.app.dto.PresolicitudDTO;
import com.utes.spring.web.app.service.EstudianteService;
import com.utes.spring.web.app.service.InscripcionService;
import com.utes.spring.web.app.service.PresolicitudService;

@Service("EstudianteService")
public class EstudianteServiceImp implements EstudianteService {

	@Autowired
	private InscripcionService inscripcionService;
	@Autowired
	private PresolicitudService presolicitudService;
	@Autowired
	private EstadoService estadoService;
		
	@Override
	public boolean habilitaBotonInscripcion(PersonaDTO enpersonasesion) {
		try {
			int valor = 0;
			if (enpersonasesion != null) {
				InscripcionDTO auxins = this.inscripcionService.obtenerInscripcionPorId(
						this.inscripcionService.obtenerInscripcionActivaMaxSecuencial());
				List<PresolicitudDTO> listpresol = this.presolicitudService.obtenerListadoPresolicitudPorCedulaId(enpersonasesion,
						auxins);
				List<EstadoDTO> auxestado = this.estadoService.loadInscripcion();
				ArrayList<Integer> errores = new ArrayList<Integer>();
				for (PresolicitudDTO objpre : listpresol) {
					if (objpre != null) {
						valor = objpre.getPslIdEstado();
						for (EstadoDTO objest : auxestado) {
							if (objest.getId() == valor) {
								errores.add(valor);
							}
						}
					}
				}
				if (errores.isEmpty()) {
					return false;
				} else {
					return true;
				}
			}

		} catch (NumberFormatException e) {
			//log.error(e);
		}
		return false;
	}

}
