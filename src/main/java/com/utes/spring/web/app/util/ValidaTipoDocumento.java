package com.utes.spring.web.app.util;

public class ValidaTipoDocumento {

	private static final Integer TIPO_CEDULA = 1;
	private static final Integer TIPO_RUC_NATURAL = 2;
	private static final Integer RUC_PRIVADA = 3;
	private static final Integer RUC_PUBLICA = 4;

	/**
	 *
	 * @return
	 */
	public static Integer getTipoCedula() {
		return TIPO_CEDULA;
	}

	/**
	 *
	 * @return
	 */
	public static Integer getTipoRucNatural() {
		return TIPO_RUC_NATURAL;
	}

	/**
	 *
	 * @return
	 */
	public static Integer getRucPrivada() {
		return RUC_PRIVADA;
	}

	/**
	 *
	 * @return
	 */
	public static Integer getRucPublica() {
		return RUC_PUBLICA;
	}
}
