package com.utes.spring.web.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.utes.spring.web.app.entity.SysUsuario;
import com.utes.spring.web.app.projection.SysUsuarioProjection;

@Repository
public interface SysUsuarioRepository extends JpaRepository<SysUsuario, Integer> {

	SysUsuario findByUsrUsuarioAndUsrActivo(String usrUsuario, boolean usrActivo);

	SysUsuario findByUsrUsuarioAndUsrClaveAndUsrActivo(String usrUsuario, String usrClave, boolean usrActivo);

	List<SysUsuario> findByUsrUsuarioLikeAndUsrActivo(String usrUsuario, boolean usrActivo);

	List<SysUsuario> findByIdUsr(Integer idUsr);

	SysUsuario findByPersonaIdPer(Integer idPer);

	List<SysUsuario> findBySysPerfilPrfNombre(String prfNombre);

	List<SysUsuario> findBySysPerfilPrfNombreAndPersonaIdPerNot(String prfNombre, Integer idPer);

	SysUsuario findByUsrCorreoAndUsrActivo(String usrCorreo, boolean usrActivo);

//	@Query(value = " SELECT	* 																			"
//			+ " from 	sys_usuario USU inner join sys_perfil PER ON USU.fk_prf = PER.id_prf        "
//			+ " where 	PER.prf_nombre = 'DOCENTE'                                                  "
//			+ " 	AND fk_per NOT IN (select fk_per FROM tema t WHERE t.id_tem = 217)              "
//			+ " 	AND fk_per IN (select fk_per FROM seleccion s WHERE s.fk_prd = 6)               ",
//			nativeQuery = true)
//	List<SysUsuario> obtenerListadoUsuarioPorPerfilAsignadoIdTema(@Param("perfilnombre") String perfilnombre, @Param("idtem") Integer idtem,
//			@Param("idtipoasignado") Integer idtipoasignado, @Param("idestado") Integer idestado, @Param("idperiodo") Integer idperiodo);

	@Query("SELECT usu FROM SysUsuario usu JOIN usu.sysPerfil prf JOIN usu.persona per"
			+ " WHERE prf.prfNombre =:perfil AND per.idPer NOT IN (SELECT t.persona.idPer FROM Tema t WHERE t.idTem =:tema) "
			+ " AND per.idPer IN (SELECT s.persona.idPer FROM Seleccion s WHERE s.periodo.idPrd =:periodo) ")
	List<SysUsuario> obtenerListadoUsuarioPorPerfilAsignadoIdTema(@Param("perfil") String perfilnombre,
			@Param("tema") Integer idtem, @Param("periodo") Integer idperiodo);

	@Query(value = " SELECT 	id_usr as idUsr, fk_per as idPersona, fk_prf as idSysPerfil, usr_usuario as usrUsuario, usr_clave as usrClave, 							"
			+ " 	usr_correo as usrCorreo, usr_activo as usrActivo, usr_fechacreado as usrFechaCreado, usr_fechaeditado as usrFechaEditado                       		"
			+ " FROM	sys_usuario SU                                                                                                                                  "
			+ " 	INNER JOIN sys_perfil SP ON SU.fk_prf = SP.id_prf                                                                                                   "
			+ " WHERE	SP.prf_nombre = :perfil                                                                                                                       	"
			+ " 		AND (:usrempty is null OR SU.id_usr = :idusr)                                                                                                      "
			+ " 		AND fk_per in (SELECT T.fk_per FROM tema T WHERE T.tem_idestado in :estados)                                                                    ", nativeQuery = true)
	List<SysUsuarioProjection> obtenerListadoUsuarioPorTemaPorPerfil(@Param("perfil") String perfilnombre,
			@Param("idusr") Integer idusr, @Param("estados") List<Integer> estados, @Param("usrempty") String usuarioIsEmpty);

	@Query(value = " SELECT 	id_usr as idUsr, fk_per as idPersona, fk_prf as idSysPerfil, usr_usuario as usrUsuario, usr_clave as usrClave,					"
			+ " 	usr_correo as usrCorreo, usr_activo as usrActivo, usr_fechacreado as usrFechaCreado, usr_fechaeditado as usrFechaEditado                    "
			+ " FROM	sys_usuario SU                                                                                                                          "
			+ " 	INNER JOIN sys_perfil SP ON SU.fk_prf = SP.id_prf                                                                                           "
			+ " WHERE	SP.prf_nombre = :perfil                                                                                                                 "
			+ " 	AND SU.fk_per in (SELECT P.fk_per FROM presolicitud P WHERE P.psl_idestado in :estados OR P.psl_idopcion in :opciones)                      "
			+ " 	AND SU.fk_per not in (SELECT A.fk_per FROM asignado A)                                                                                      ", nativeQuery = true)
	List<SysUsuarioProjection> obtenerListadoUsuarioPorPerfilPresolicitud(@Param("perfil") String perfilnombre,
			@Param("estados") List<Integer> estados, @Param("opciones") List<Integer> opciones);

	@Query(value = " SELECT 	id_usr as idUsr, fk_per as idPersona, fk_prf as idSysPerfil, usr_usuario as usrUsuario, usr_clave as usrClave, 					"
			+ " 	usr_correo as usrCorreo, usr_activo as usrActivo, usr_fechacreado as usrFechaCreado, usr_fechaeditado as usrFechaEditado                    "
			+ " FROM	sys_usuario SU                                                                                                                          "
			+ " 	INNER JOIN sys_perfil SP ON SU.fk_prf = SP.id_prf                                                                                           "
			+ " WHERE	SP.prf_nombre = :perfil                                                                                                            		"
			+ " 	AND SU.fk_per not in (SELECT S.fk_per FROM seleccion S WHERE S.fk_prd = (SELECT P.id_prd                                                    "
			+ " 																			 FROM periodo P WHERE P.prd_numero = :prdnum ))                     ", nativeQuery = true)
	List<SysUsuarioProjection> obtenerListadoUsuarioPorPerfilSeleccion(@Param("perfil") String perfilnombre,
			@Param("prdnum") Integer prdnum);

	@Query(value = " SELECT 	id_usr as idUsr, fk_per as idPersona, fk_prf as idSysPerfil, usr_usuario as usrUsuario, usr_clave as usrClave, 					"
			+ " 	usr_correo as usrCorreo, usr_activo as usrActivo, usr_fechacreado as usrFechaCreado, usr_fechaeditado as usrFechaEditado                    "
			+ " FROM	sys_usuario SU                                                                                                                          "
			+ " 	INNER JOIN sys_perfil SP ON SU.fk_prf = SP.id_prf                                                                                           "
			+ " WHERE	SP.prf_nombre = :perfil                                                                                                            		"
			+ " 	AND SU.fk_per in (SELECT 	P.fk_per                                                                                                        "
			+ " 					  FROM 		presolicitud P                                                                                                  "
			+ " 					  WHERE 	(P.psl_idestado in :estados OR P.psl_idopcion in :opciones) AND P.fk_ins = :idinscripcion)	                    "
			+ " 	AND SU.fk_per not in (SELECT A.fk_per FROM asignado A WHERE A.asg_activo = true )                                                           ", nativeQuery = true)
	List<SysUsuarioProjection> obtenerListadoUsuarioPorPerfilPresolicitud(@Param("perfil") String perfilnombre,
			@Param("idinscripcion") Integer idinscripcion, @Param("estados") List<Integer> estados,
			@Param("opciones") List<Integer> opciones);

	@Query(value = " SELECT 	id_usr as idUsr, fk_per as idPersona, fk_prf as idSysPerfil, usr_usuario as usrUsuario, usr_clave as usrClave, 					"
			+ " 	usr_correo as usrCorreo, usr_activo as usrActivo, usr_fechacreado as usrFechaCreado, usr_fechaeditado as usrFechaEditado                    "
			+ " FROM	sys_usuario SU                                                                                                                          "
			+ " 	INNER JOIN sys_perfil SP ON SU.fk_prf = SP.id_prf                                                                                           "
			+ " WHERE	SP.prf_nombre = :perfil                                                                                                               	"
			+ " 		AND (:usrempty is null OR SU.id_usr = :idusr)                                                                                           "
			+ " 		AND fk_per in (SELECT T.fk_per FROM tema T WHERE T.tem_idestado in :estados AND T.tem_idperiodo = :idperiodo)                           ", nativeQuery = true)
	List<SysUsuarioProjection> obtenerListadoUsuarioPorTemaPorPerfil2(@Param("perfil") String perfilnombre,
			@Param("idusr") Integer idusr, @Param("idperiodo") Integer idperiodo,
			@Param("estados") List<Integer> estados, @Param("usrempty") String usuarioIsEmpty);

}
