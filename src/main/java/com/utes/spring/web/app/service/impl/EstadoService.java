package com.utes.spring.web.app.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.utes.spring.web.app.EstaticosConfig;
import com.utes.spring.web.app.dto.EstadoDTO;

@Service
public class EstadoService {

	@Autowired
	EstaticosConfig estaticos = new EstaticosConfig();
	
	public List<EstadoDTO> loadPreTema() {
		EstadoDTO e = new EstadoDTO(this.estaticos.getESTADO_TEMA_PRE_CREADO(), estaticos.getESTADO_LABEL_TEMA_CREADO(), estaticos.getESTADO_FASE_PRERESOLUCION());
        
		return new ArrayList<EstadoDTO>() {{
            add(new EstadoDTO(estaticos.getESTADO_TEMA_PRE_CREADO(), estaticos.getESTADO_LABEL_TEMA_CREADO(), estaticos.getESTADO_FASE_PRERESOLUCION()));
            add(new EstadoDTO(estaticos.getESTADO_TEMA_PRE_ENVIADO(), estaticos.getESTADO_LABEL_TEMA_ENVIADO(), estaticos.getESTADO_FASE_PRERESOLUCION()));
            add(new EstadoDTO(estaticos.getESTADO_TEMA_PRE_ASIGNAREVISOR(), estaticos.getESTADO_LABEL_TEMA_ASIGNAREVISOR(), estaticos.getESTADO_FASE_PRERESOLUCION()));
            add(new EstadoDTO(estaticos.getESTADO_TEMA_PRE_REVISION(), estaticos.getESTADO_LABEL_TEMA_ENREVISION(), estaticos.getESTADO_FASE_PRERESOLUCION()));
            add(new EstadoDTO(estaticos.getESTADO_TEMA_PRE_REVISADO(), estaticos.getESTADO_LABEL_TEMA_REVISADO(), estaticos.getESTADO_FASE_PRERESOLUCION()));
            add(new EstadoDTO(estaticos.getESTADO_TEMA_PRE_PUBLICADO(), estaticos.getESTADO_LABEL_TEMA_PUBLICADO(), estaticos.getESTADO_FASE_PRERESOLUCION()));
            add(new EstadoDTO(estaticos.getESTADO_TEMA_PRE_ASIGNAESTUDIANTE(), estaticos.getESTADO_LABEL_TEMA_ASIGNAESTUDIANTE(), estaticos.getESTADO_FASE_PRERESOLUCION()));
            add(new EstadoDTO(estaticos.getESTADO_TEMA_PRE_ASIGNALECTOR(), estaticos.getESTADO_LABEL_TEMA_ASIGNALECTOR(), estaticos.getESTADO_FASE_PRERESOLUCION()));
            add(new EstadoDTO(estaticos.getESTADO_TEMA_PRE_TERMINALECTOR(), estaticos.getESTADO_LABEL_TEMA_TERMINALECTOR(), estaticos.getESTADO_FASE_PRERESOLUCION()));
        }};
    }

    public List<EstadoDTO> loadPostTema() {
        return new ArrayList<EstadoDTO>() {{
            add(new EstadoDTO(estaticos.getESTADO_TEMA_POST_APROBADO(), estaticos.getESTADO_LABEL_TEMA_APROBADO(), estaticos.getESTADO_FASE_POSTRESOLUCION()));
            add(new EstadoDTO(estaticos.getESTADO_TEMA_POST_CERRADO(), estaticos.getESTADO_LABEL_TEMA_CERRADO(), estaticos.getESTADO_FASE_POSTRESOLUCION()));
            add(new EstadoDTO(estaticos.getESTADO_TEMA_POST_ANULADO(), estaticos.getESTADO_LABEL_TEMA_ANULADO(), estaticos.getESTADO_FASE_POSTRESOLUCION()));
            add(new EstadoDTO(estaticos.getESTADO_TEMA_POST_PRORROGA(), estaticos.getESTADO_LABEL_TEMA_PRORROGA(), estaticos.getESTADO_FASE_POSTRESOLUCION()));
            add(new EstadoDTO(estaticos.getESTADO_TEMA_POST_LECTORPROYECTO(), estaticos.getESTADO_LABEL_TEMA_LECTORPROYECTO(), estaticos.getESTADO_FASE_POSTRESOLUCION()));
            add(new EstadoDTO(estaticos.getESTADO_TEMA_POST_CAMBIOTUTOR(), estaticos.getESTADO_LABEL_TEMA_CAMBIOTUTOR(), estaticos.getESTADO_FASE_POSTRESOLUCION()));
            add(new EstadoDTO(estaticos.getESTADO_TEMA_POST_CAMBIOTEMA(), estaticos.getESTADO_LABEL_TEMA_CAMBIOTEMA(), estaticos.getESTADO_FASE_POSTRESOLUCION()));
            add(new EstadoDTO(estaticos.getESTADO_TEMA_POST_RENUNCIAESTUDIANTE(), estaticos.getESTADO_LABEL_TEMA_RENUNCIAESTUDIANTE(), estaticos.getESTADO_FASE_POSTRESOLUCION()));
        }};
    }

    public List<EstadoDTO> loadPostTemaAccion() {
        return new ArrayList<EstadoDTO>() {{
            add(new EstadoDTO(estaticos.getESTADO_TEMA_POST_CERRADO(), estaticos.getESTADO_LABEL_TEMA_CERRADO(), estaticos.getESTADO_FASE_POSTRESOLUCION()));
            add(new EstadoDTO(estaticos.getESTADO_TEMA_POST_ANULADO(), estaticos.getESTADO_LABEL_TEMA_ANULADO(), estaticos.getESTADO_FASE_POSTRESOLUCION()));
            add(new EstadoDTO(estaticos.getESTADO_TEMA_POST_PRORROGA(), estaticos.getESTADO_LABEL_TEMA_PRORROGA(), estaticos.getESTADO_FASE_POSTRESOLUCION()));
        }};
    }

    public List<EstadoDTO> loadInscripcion() {
        return new ArrayList<EstadoDTO>() {{
            add(new EstadoDTO(estaticos.getESTADO_PRESOLICITUD_ENVIADO(), estaticos.getESTADO_LABEL_PRESOLICITUD_ENVIADO(), estaticos.getESTADO_FASE_PRESOLICITUD()));
            add(new EstadoDTO(estaticos.getESTADO_PRESOLICITUD_APROBADO(), estaticos.getESTADO_LABEL_PRESOLICITUD_APROBADO(), estaticos.getESTADO_FASE_PRESOLICITUD()));
            add(new EstadoDTO(estaticos.getESTADO_PRESOLICITUD_ENLISTAESPERA(), estaticos.getESTADO_LABEL_PRESOLICITUD_LISTAESPERA(), estaticos.getESTADO_FASE_PRESOLICITUD()));
            add(new EstadoDTO(estaticos.getESTADO_PRESOLICITUD_NEGADO(), estaticos.getESTADO_LABEL_PRESOLICITUD_NEGADO(), estaticos.getESTADO_FASE_PRESOLICITUD()));
            add(new EstadoDTO(estaticos.getESTADO_PRESOLICITUD_PREREVISADO(), estaticos.getESTADO_LABEL_PRESOLICITUD_REVISIONPREVIA(), estaticos.getESTADO_FASE_PRESOLICITUD()));
            add(new EstadoDTO(estaticos.getESTADO_PRESOLICITUD_FALTA(), estaticos.getESTADO_LABEL_PRESOLICITUD_FALTA(), estaticos.getESTADO_FASE_PRESOLICITUD()));
            add(new EstadoDTO(estaticos.getESTADO_PRESOLICITUD_RENUNCIA(), estaticos.getESTADO_LABEL_PRESOLICITUD_RENUNCIA(), estaticos.getESTADO_FASE_PRESOLICITUD()));
        }};
    }

    public List<EstadoDTO> loadInscripcionAccion() {
        return new ArrayList<EstadoDTO>() {{
            add(new EstadoDTO(estaticos.getESTADO_PRESOLICITUD_APROBADO(), estaticos.getESTADO_LABEL_PRESOLICITUD_APROBADO(), estaticos.getESTADO_FASE_PRESOLICITUD()));
            add(new EstadoDTO(estaticos.getESTADO_PRESOLICITUD_ENLISTAESPERA(), estaticos.getESTADO_LABEL_PRESOLICITUD_LISTAESPERA(), estaticos.getESTADO_FASE_PRESOLICITUD()));
            add(new EstadoDTO(estaticos.getESTADO_PRESOLICITUD_NEGADO(), estaticos.getESTADO_LABEL_PRESOLICITUD_NEGADO(), estaticos.getESTADO_FASE_PRESOLICITUD()));
        }};
    }

    public List<EstadoDTO> loadInscripcionRevision() {
        return new ArrayList<EstadoDTO>() {{
            add(new EstadoDTO(estaticos.getESTADO_PRESOLICITUD_PREREVISADO(), estaticos.getESTADO_LABEL_PRESOLICITUD_REVISIONPREVIA(), estaticos.getESTADO_FASE_PRESOLICITUD()));
            add(new EstadoDTO(estaticos.getESTADO_PRESOLICITUD_FALTA(), estaticos.getESTADO_LABEL_PRESOLICITUD_FALTA(), estaticos.getESTADO_FASE_PRESOLICITUD()));
        }};
    }

    public List<EstadoDTO> loadEvolucion() {
        return new ArrayList<EstadoDTO>() {{
            add(new EstadoDTO(estaticos.getESTADO_EVOLUCION_CREADO(), estaticos.getESTADO_LABEL_EVOLUCION_CREADO(), estaticos.getESTADO_FASE_EVOLUCION()));
            add(new EstadoDTO(estaticos.getESTADO_EVOLUCION_ASISTENCIA(), estaticos.getESTADO_LABEL_EVOLUCION_ASISTENCIA(), estaticos.getESTADO_FASE_EVOLUCION()));
            add(new EstadoDTO(estaticos.getESTADO_EVOLUCION_NOASISTENCIA(), estaticos.getESTADO_LABEL_EVOLUCION_NOASISTENCIA(), estaticos.getESTADO_FASE_EVOLUCION()));
            add(new EstadoDTO(estaticos.getESTADO_EVOLUCION_REAGENDA(), estaticos.getESTADO_LABEL_EVOLUCION_REAGENDA(), estaticos.getESTADO_FASE_EVOLUCION()));
        }};
    }

    public List<EstadoDTO> loadEvolucionAccion() {
        return new ArrayList<EstadoDTO>() {{
            add(new EstadoDTO(estaticos.getESTADO_EVOLUCION_ASISTENCIA(), estaticos.getESTADO_LABEL_EVOLUCION_ASISTENCIA(), estaticos.getESTADO_FASE_EVOLUCION()));
            add(new EstadoDTO(estaticos.getESTADO_EVOLUCION_NOASISTENCIA(), estaticos.getESTADO_LABEL_EVOLUCION_NOASISTENCIA(), estaticos.getESTADO_FASE_EVOLUCION()));
            add(new EstadoDTO(estaticos.getESTADO_EVOLUCION_REAGENDA(), estaticos.getESTADO_LABEL_EVOLUCION_REAGENDA(), estaticos.getESTADO_FASE_EVOLUCION()));
        }};
    }

    public List<EstadoDTO> loadHito() {
        return new ArrayList<EstadoDTO>() {{
            add(new EstadoDTO(estaticos.getESTADO_HITO_CREADO(), estaticos.getESTADO_LABEL_HITO_CREADO(), estaticos.getESTADO_FASE_HITO()));
            add(new EstadoDTO(estaticos.getESTADO_HITO_CUMPLE(), estaticos.getESTADO_LABEL_HITO_CUMPLIDO(), estaticos.getESTADO_FASE_HITO()));
            add(new EstadoDTO(estaticos.getESTADO_HITO_NOCUMPLE(), estaticos.getESTADO_LABEL_HITO_NOCUMPLIDO(), estaticos.getESTADO_FASE_HITO()));
        }};
    }

    public List<EstadoDTO> loadHitoAccion() {
        return new ArrayList<EstadoDTO>() {{
            add(new EstadoDTO(estaticos.getESTADO_HITO_CUMPLE(), estaticos.getESTADO_LABEL_HITO_CUMPLIDO(), estaticos.getESTADO_FASE_HITO()));
            add(new EstadoDTO(estaticos.getESTADO_HITO_NOCUMPLE(), estaticos.getESTADO_LABEL_HITO_NOCUMPLIDO(), estaticos.getESTADO_FASE_HITO()));
        }};
    }

    public List<EstadoDTO> loadAsignaLector() {
        return new ArrayList<EstadoDTO>() {{
            add(new EstadoDTO(estaticos.getESTADO_ID_TEMA_LECTOR_PROCESO(), estaticos.getESTADO_LABEL_TEMA_LECTOR_PROCESO(), estaticos.getESTADO_FASE_ASIGNACION()));
            add(new EstadoDTO(estaticos.getESTADO_ID_TEMA_LECTOR_TERMINADO(), estaticos.getESTADO_LABEL_TEMA_LECTOR_TERMINADO(), estaticos.getESTADO_FASE_ASIGNACION()));
        }};
    }

    public List<EstadoDTO> loadAsignaRevisor() {
        return new ArrayList<EstadoDTO>() {{
            add(new EstadoDTO(estaticos.getESTADO_ID_TEMA_REVISA_PROCESO(), estaticos.getESTADO_LABEL_TEMA_REVISA_PROCESO(), estaticos.getESTADO_FASE_ASIGNACION()));
            add(new EstadoDTO(estaticos.getESTADO_ID_TEMA_REVISA_REVISADO(), estaticos.getESTADO_LABEL_TEMA_REVISA_REVISADO(), estaticos.getESTADO_FASE_ASIGNACION()));
            add(new EstadoDTO(estaticos.getESTADO_ID_TEMA_REVISA_TERMINADO(), estaticos.getESTADO_LABEL_TEMA_REVISA_TERMINADO(), estaticos.getESTADO_FASE_ASIGNACION()));
        }};
    }

    public List<EstadoDTO> loadPrePostTema() {
    	List<EstadoDTO> list = new ArrayList<>();
    	list.addAll(this.loadPostTema());
    	list.addAll(this.loadPreTema());
        return list;
    }


    public List<EstadoDTO> loadAsignaLectorRevisor() {
    	List<EstadoDTO> list = new ArrayList<>();
    	list.addAll(this.loadAsignaLector());
    	list.addAll(this.loadAsignaRevisor());
        return list;
    }

    public List<EstadoDTO> loadRevisorTema() {
        return new ArrayList<EstadoDTO>() {{
            add(new EstadoDTO(estaticos.getESTADO_TEMA_PRE_ENVIADO(), estaticos.getESTADO_LABEL_TEMA_ENVIADO(), estaticos.getESTADO_FASE_PRERESOLUCION()));
            add(new EstadoDTO(estaticos.getESTADO_TEMA_PRE_ASIGNAREVISOR(), estaticos.getESTADO_LABEL_TEMA_ASIGNAREVISOR(), estaticos.getESTADO_FASE_PRERESOLUCION()));
            add(new EstadoDTO(estaticos.getESTADO_TEMA_PRE_REVISION(), estaticos.getESTADO_LABEL_TEMA_ENREVISION(), estaticos.getESTADO_FASE_PRERESOLUCION()));
            add(new EstadoDTO(estaticos.getESTADO_TEMA_PRE_REVISADO(), estaticos.getESTADO_LABEL_TEMA_REVISADO(), estaticos.getESTADO_FASE_PRERESOLUCION()));
            add(new EstadoDTO(estaticos.getESTADO_TEMA_PRE_PUBLICADO(), estaticos.getESTADO_LABEL_TEMA_PUBLICADO(), estaticos.getESTADO_FASE_PRERESOLUCION()));
        }};
    }

    public List<EstadoDTO> loadRevisorInforme() {
        return new ArrayList<EstadoDTO>() {{
            add(new EstadoDTO(estaticos.getESTADO_TEMA_PRE_REVISION(), estaticos.getESTADO_LABEL_TEMA_ENREVISION(), estaticos.getESTADO_FASE_PRERESOLUCION()));
            add(new EstadoDTO(estaticos.getESTADO_TEMA_PRE_REVISADO(), estaticos.getESTADO_LABEL_TEMA_REVISADO(), estaticos.getESTADO_FASE_PRERESOLUCION()));
        }};
    }

	public String getNombreEstadoPorLista(int id, List<EstadoDTO> listaestado) {
		String data = "";
		if (id != 0) {
			for (EstadoDTO estado : listaestado) {
				if (estado.getId() == id) {
					data = estado.getTitulo();
					break;
				}
			}
		}
		return data;
	}
	
	public String getListaString(List<EstadoDTO> listaestado) {
		String data = "";
		for (EstadoDTO estado : listaestado) {
			data.concat(String.valueOf(estado.getId())).concat(",");
		}
		return data;
	}
}
