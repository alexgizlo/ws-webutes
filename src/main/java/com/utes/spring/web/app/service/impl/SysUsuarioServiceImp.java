package com.utes.spring.web.app.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.utes.spring.web.app.dto.LoginRegisterDTO;
import com.utes.spring.web.app.dto.PersonaDTO;
import com.utes.spring.web.app.dto.SysPerfilDTO;
import com.utes.spring.web.app.dto.SysUsuarioDTO;
import com.utes.spring.web.app.entity.Persona;
import com.utes.spring.web.app.entity.SysPerfil;
import com.utes.spring.web.app.entity.SysUsuario;
import com.utes.spring.web.app.projection.SysUsuarioProjection;
import com.utes.spring.web.app.repository.PersonaRepository;
import com.utes.spring.web.app.repository.SysPerfilRepository;
import com.utes.spring.web.app.repository.SysUsuarioRepository;
import com.utes.spring.web.app.service.PersonaService;
import com.utes.spring.web.app.service.SysPerfilService;
import com.utes.spring.web.app.util.Seguridad;
import com.utes.spring.web.app.util.Valida;

@Service("SysUsuarioService")
public class SysUsuarioServiceImp implements com.utes.spring.web.app.service.SysUsuarioService {

	@Autowired
	private SysUsuarioRepository sysUsuarioRepository;
	@Autowired
	private PersonaRepository personaRepository;
	@Autowired
	private SysPerfilRepository sysPerfilRepository;
	@Autowired
	private PersonaService personaService;
	@Autowired
	private SysPerfilService sysPerfilService;
	private final String key = "92AE31A79FEEB2A3"; // llave
	private final String iv = "0123456789ABCDEF";

	@Override
	@Transactional
	public boolean create(SysUsuarioDTO obj) {
		boolean success = false;
		try {
			SysUsuario sysUsuarioBD = new SysUsuario();
			obj.setUsrClave(Seguridad.encrypt(this.key, this.iv, obj.getUsrClave()));
			this.convertirDtoToEntity(obj, sysUsuarioBD);
			this.sysUsuarioRepository.save(sysUsuarioBD);
			success = true;
		} catch (final Exception e) {
			success = false;
		}
		return success;
	}

	@Override
	@Transactional
	public boolean update(SysUsuarioDTO obj) {
		boolean success = false;
		try {
			SysUsuario sysUsuarioBD = this.sysUsuarioRepository.findById(obj.getIdUsr()).orElse(null);
			if (sysUsuarioBD != null) {
				obj.setUsrClave(Seguridad.encrypt(this.key, this.iv, obj.getUsrClave()));
				this.convertirDtoToEntity(obj, sysUsuarioBD);
				this.sysUsuarioRepository.save(sysUsuarioBD);
				success = true;
			}
		} catch (final Exception e) {
			success = false;
		}
		return success;
	}

	@Override
	@Transactional
	public boolean delete(Integer id) {
		boolean success = false;
		try {
			SysUsuario sysUsuarioBD = this.sysUsuarioRepository.findById(id).orElse(null);
			if (sysUsuarioBD != null) {
				this.sysUsuarioRepository.delete(sysUsuarioBD);
				success = true;
			}
		} catch (final Exception e) {
			success = false;
		}
		return success;
	}

	@Override
	@Transactional(readOnly = true)
	public List<SysUsuarioDTO> obtenerListadoUsuario() {
		final List<SysUsuario> listSysUsuariosBD = this.sysUsuarioRepository.findAll();
		final List<SysUsuarioDTO> resultado = new ArrayList<SysUsuarioDTO>();
		if (listSysUsuariosBD != null && !listSysUsuariosBD.isEmpty()) {
			for (final SysUsuario sysUsuario : listSysUsuariosBD) {
				resultado.add(this.convertirEntityToDto(sysUsuario, true, false));
			}
		}
		return resultado;
	}

	@Override
	@Transactional(readOnly = true)
	public SysUsuarioDTO loginUsuario(SysUsuarioDTO auxobj) {
		String password = auxobj.getUsrClave();
		try {
			auxobj.setUsrClave(Seguridad.encrypt(this.key, this.iv, password));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		SysUsuario sysUsuarioBD = this.sysUsuarioRepository
				.findByUsrUsuarioAndUsrClaveAndUsrActivo(auxobj.getUsrUsuario(), auxobj.getUsrClave(), true);
		if (sysUsuarioBD != null) {
			return this.convertirEntityToDto(sysUsuarioBD, true, true);
		}
		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public SysUsuarioDTO obtenerUsuarioPorId(Integer pk) {
		SysUsuario sysUsuarioBD = this.sysUsuarioRepository.findById(pk).orElse(null);
		if (sysUsuarioBD != null) {
			return this.convertirEntityToDto(sysUsuarioBD, true, true);
		}
		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public SysUsuarioDTO obtenerUsuarioPorNombreUsuario(String nombreusuario) {
		SysUsuario sysUsuarioBD = this.sysUsuarioRepository.findByUsrUsuarioAndUsrActivo(nombreusuario, true);
		if (sysUsuarioBD != null) {
			return this.convertirEntityToDto(sysUsuarioBD, true, true);
		}
		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public List<SysUsuarioDTO> obtenerListadoUsuarioPorNombre(String nombreusuario) {
		final List<SysUsuario> listSysUsuariosBD = this.sysUsuarioRepository
				.findByUsrUsuarioLikeAndUsrActivo(nombreusuario, true);
		final List<SysUsuarioDTO> resultado = new ArrayList<SysUsuarioDTO>();
		if (listSysUsuariosBD != null && !listSysUsuariosBD.isEmpty()) {
			for (final SysUsuario sysUsuario : listSysUsuariosBD) {
				resultado.add(this.convertirEntityToDto(sysUsuario, true, false));
			}
		}
		return resultado;
	}

	@Override
	@Transactional(readOnly = true)
	public List<SysUsuarioDTO> obtenerListadoUsuarioPorUsuarioNombre(String nombreusuario) {
		final List<SysUsuario> listSysUsuariosBD = this.sysUsuarioRepository
				.findByUsrUsuarioLikeAndUsrActivo(nombreusuario, true);
		final List<SysUsuarioDTO> resultado = new ArrayList<SysUsuarioDTO>();
		if (listSysUsuariosBD != null && !listSysUsuariosBD.isEmpty()) {
			for (final SysUsuario sysUsuario : listSysUsuariosBD) {
				resultado.add(this.convertirEntityToDto(sysUsuario, true, false));
			}
		}
		return resultado;
	}

	@Override
	@Transactional(readOnly = true)
	public SysUsuarioDTO obtenerUsuarioPorPersona(PersonaDTO objpersona) {
		SysUsuario sysUsuarioBD = this.sysUsuarioRepository.findByPersonaIdPer(objpersona.getIdPer());
		if (sysUsuarioBD != null) {
			return this.convertirEntityToDto(sysUsuarioBD, true, true);
		}
		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public SysUsuarioDTO obtenerUsuarioPorPersonaId(Integer idpersona) {
		SysUsuario sysUsuarioBD = this.sysUsuarioRepository.findByPersonaIdPer(idpersona);
		if (sysUsuarioBD != null) {
			SysUsuarioDTO sysUsuarioDTO = this.convertirEntityToDto(sysUsuarioBD, true, true);
			return sysUsuarioDTO;
		}
		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public List<SysUsuarioDTO> obtenerListadoUsuarioPorPerfil(String perfilnombre) {
		final List<SysUsuario> listSysUsuariosBD = this.sysUsuarioRepository.findBySysPerfilPrfNombre(perfilnombre);
		final List<SysUsuarioDTO> resultado = new ArrayList<SysUsuarioDTO>();
		if (listSysUsuariosBD != null && !listSysUsuariosBD.isEmpty()) {
			for (final SysUsuario sysUsuario : listSysUsuariosBD) {
				resultado.add(this.convertirEntityToDto(sysUsuario, true, false));
			}
		}
		return resultado;
	}

	@Override
	@Transactional(readOnly = true)
	public List<SysUsuarioDTO> obtenerListadoUsuarioPorPerfilPresolicitud(String perfilnombre, String preslestados,
			String preslopciones) {
		List<Integer> listestados = new ArrayList<>();
		String[] s = preslestados.split(",");
		if (!preslestados.isEmpty()) {
			for (int index = 0; index < s.length; index++) {
				listestados.add(Integer.parseInt(s[index]));
			}
		}
		List<Integer> listopciones = new ArrayList<>();
		String[] s2 = preslopciones.split(",");
		if (!preslopciones.isEmpty()) {
			for (int index = 0; index < s2.length; index++) {
				listopciones.add(Integer.parseInt(s2[index]));
			}
		}
		final List<SysUsuarioProjection> listSysUsuariosBD = this.sysUsuarioRepository
				.obtenerListadoUsuarioPorPerfilPresolicitud(perfilnombre, listestados, listopciones);
		final List<SysUsuarioDTO> resultado = new ArrayList<SysUsuarioDTO>();
		if (listSysUsuariosBD != null && !listSysUsuariosBD.isEmpty()) {
			for (final SysUsuarioProjection sysUsuario : listSysUsuariosBD) {
				resultado.add(this.convertirProjectionToDto(sysUsuario, true, false));
			}
		}
		return resultado;
	}

	@Override
	@Transactional(readOnly = true)
	public List<SysUsuarioDTO> obtenerListadoUsuarioPorPerfilAsignado(String perfilnombre, Integer idtem,
			Integer idperiodo) {
		final List<SysUsuario> listSysUsuariosBD = this.sysUsuarioRepository
				.obtenerListadoUsuarioPorPerfilAsignadoIdTema(perfilnombre, idtem, idperiodo);
		final List<SysUsuarioDTO> resultado = new ArrayList<SysUsuarioDTO>();
		if (listSysUsuariosBD != null && !listSysUsuariosBD.isEmpty()) {
			for (final SysUsuario sysUsuario : listSysUsuariosBD) {
				resultado.add(this.convertirEntityToDto(sysUsuario, true, false));
			}
		}
		return resultado;
	}

	@Override
	@Transactional(readOnly = true)
	public List<SysUsuarioDTO> obtenerListadoUsuarioPorPerfilAsignado(String perfilnombre) {
		final List<SysUsuario> listSysUsuariosBD = this.sysUsuarioRepository.findBySysPerfilPrfNombre(perfilnombre);
		final List<SysUsuarioDTO> resultado = new ArrayList<SysUsuarioDTO>();
		if (listSysUsuariosBD != null && !listSysUsuariosBD.isEmpty()) {
			for (final SysUsuario sysUsuario : listSysUsuariosBD) {
				resultado.add(this.convertirEntityToDto(sysUsuario, true, false));
			}
		}
		return resultado;
	}

	@Override
	@Transactional(readOnly = true)
	public List<SysUsuarioDTO> obtenerListadoUsuarioPorPerfilSeleccion(String perfilnombre, Integer periodo) {
		final List<SysUsuarioProjection> listSysUsuariosBD = this.sysUsuarioRepository
				.obtenerListadoUsuarioPorPerfilSeleccion(perfilnombre, periodo);
		final List<SysUsuarioDTO> resultado = new ArrayList<SysUsuarioDTO>();
		if (listSysUsuariosBD != null && !listSysUsuariosBD.isEmpty()) {
			for (final SysUsuarioProjection sysUsuario : listSysUsuariosBD) {
				resultado.add(this.convertirProjectionToDto(sysUsuario, true, false));
			}
		}
		return resultado;
	}

	@Override
	@Transactional(readOnly = true)
	public List<SysUsuarioDTO> obtenerListadoUsuarioPorPerfil(String perfilnombre, Integer idpersonatutor) {
		final List<SysUsuario> listSysUsuariosBD = this.sysUsuarioRepository
				.findBySysPerfilPrfNombreAndPersonaIdPerNot(perfilnombre, idpersonatutor);
		final List<SysUsuarioDTO> resultado = new ArrayList<SysUsuarioDTO>();
		if (listSysUsuariosBD != null && !listSysUsuariosBD.isEmpty()) {
			for (final SysUsuario sysUsuario : listSysUsuariosBD) {
				resultado.add(this.convertirEntityToDto(sysUsuario, true, false));
			}
		}
		return resultado;
	}

	@Override
	@Transactional(readOnly = true)
	public List<SysUsuarioDTO> obtenerListadoUsuarioPorUsuarioId(Integer idusuario) {
		final List<SysUsuario> listSysUsuariosBD = this.sysUsuarioRepository.findByIdUsr(idusuario);
		final List<SysUsuarioDTO> resultado = new ArrayList<SysUsuarioDTO>();
		if (listSysUsuariosBD != null && !listSysUsuariosBD.isEmpty()) {
			for (final SysUsuario sysUsuario : listSysUsuariosBD) {
				resultado.add(this.convertirEntityToDto(sysUsuario, true, false));
			}
		}
		return resultado;
	}

	@Override
	@Transactional(readOnly = true)
	public List<SysUsuarioDTO> obtenerListadoUsuarioPorTemaPorPerfil(String perfilnombre, Integer idusuario,
			String estados) {
		List<Integer> list = new ArrayList<>();
		String[] s = estados.split(",");
		if (!estados.isEmpty()) {
			for (int index = 0; index < s.length; index++) {
				list.add(Integer.parseInt(s[index]));
			}
		}
		String idUsuarioIsEmpty = "A";
		if (idusuario == null) {
			idusuario = 0;
			idUsuarioIsEmpty = null;
		}
		final List<SysUsuarioProjection> listSysUsuariosBD = this.sysUsuarioRepository
				.obtenerListadoUsuarioPorTemaPorPerfil(perfilnombre, idusuario, list, idUsuarioIsEmpty);
		final List<SysUsuarioDTO> resultado = new ArrayList<SysUsuarioDTO>();
		if (listSysUsuariosBD != null && !listSysUsuariosBD.isEmpty()) {
			for (final SysUsuarioProjection sysUsuario : listSysUsuariosBD) {
				resultado.add(this.convertirProjectionToDto(sysUsuario, true, false));
			}
		}
		return resultado;
	}

	@Override
	@Transactional(readOnly = true)
	public SysUsuarioDTO obtenerUsuarioPorCorreo(String correo) {
		SysUsuario sysUsuarioBD = this.sysUsuarioRepository.findByUsrCorreoAndUsrActivo(correo, true);
		if (sysUsuarioBD != null) {
			return this.convertirEntityToDto(sysUsuarioBD, true, true);
		}
		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public List<SysUsuarioDTO> obtenerListadoUsuarioPorPerfilPresolicitud(String perfilnombre, String preslestados,
			String preslopciones, Integer idinscripcion) {
		List<Integer> listestados = new ArrayList<>();
		String[] s = preslestados.split(",");
		if (!preslestados.isEmpty()) {
			for (int index = 0; index < s.length; index++) {
				listestados.add(Integer.parseInt(s[index]));
			}
		}
		List<Integer> listopciones = new ArrayList<>();
		String[] s2 = preslopciones.split(",");
		if (!preslopciones.isEmpty()) {
			for (int index = 0; index < s2.length; index++) {
				listopciones.add(Integer.parseInt(s2[index]));
			}
		}
		final List<SysUsuarioProjection> listSysUsuariosBD = this.sysUsuarioRepository
				.obtenerListadoUsuarioPorPerfilPresolicitud(perfilnombre, idinscripcion, listestados, listopciones);
		final List<SysUsuarioDTO> resultado = new ArrayList<SysUsuarioDTO>();
		if (listSysUsuariosBD != null && !listSysUsuariosBD.isEmpty()) {
			for (final SysUsuarioProjection sysUsuario : listSysUsuariosBD) {
				resultado.add(this.convertirProjectionToDto(sysUsuario, true, false));
			}
		}
		return resultado;
	}

	@Override
	@Transactional(readOnly = true)
	public List<SysUsuarioDTO> obtenerListadoUsuarioPorTemaPorPerfil(String perfilnombre, Integer idusuario,
			String estados, Integer idperiodo) {
		List<Integer> list = new ArrayList<>();
		String[] s = estados.split(",");
		if (!estados.isEmpty()) {
			for (int index = 0; index < s.length; index++) {
				list.add(Integer.parseInt(s[index]));
			}
		}
		String idUsuarioIsEmpty = "A";
		if (idusuario == null) {
			idusuario = 0;
			idUsuarioIsEmpty = null;
		}
		final List<SysUsuarioProjection> listSysUsuariosBD = this.sysUsuarioRepository
				.obtenerListadoUsuarioPorTemaPorPerfil2(perfilnombre, idusuario, idperiodo, list, idUsuarioIsEmpty);
		final List<SysUsuarioDTO> resultado = new ArrayList<SysUsuarioDTO>();
		if (listSysUsuariosBD != null && !listSysUsuariosBD.isEmpty()) {
			for (final SysUsuarioProjection sysUsuario : listSysUsuariosBD) {
				resultado.add(this.convertirProjectionToDto(sysUsuario, true, false));
			}
		}
		return resultado;
	}

	@Override
	@Transactional(readOnly = true)
	public boolean loginRegisterUsuario(LoginRegisterDTO auxobj) {
		boolean success = false;
		try {
			Date utilFecha = new Date();
			SysPerfilDTO auxperfil;
			if (auxobj.getSysUsuario().getIdSysPerfil() != null) {
				auxperfil = this.sysPerfilService.obtenerPerfilPorId(auxobj.getSysUsuario().getIdSysPerfil());
			} else {
				auxperfil = this.sysPerfilService.obtenerPerfilPorNombre("ESTUDIANTE");
			}
			if (auxperfil != null) {
				if (validacionCedula(auxobj.getPersona().getPerCedula()) == true) {
					if (this.personaService.obtenerPersonaPorCedula(auxobj.getPersona().getPerCedula()) != null) {
						// Mensajes.mensajeWarn(null, "La persona con el número de cedula ingresado ya
						// existe.", null);
					} else {
						List<SysUsuarioDTO> listvalidauser = obtenerListadoUsuarioPorUsuarioNombre(
								auxobj.getSysUsuario().getUsrUsuario());
						if (listvalidauser.isEmpty() != true) {
							// Mensajes.mensajeError(null, "El registro del usuario ya existe.", null);
						} else {
							auxobj.getPersona().setPerFechaCreado(utilFecha);
							auxobj.getPersona().setPerFechaEditado(utilFecha);
							PersonaDTO persona = this.personaService.create2(auxobj.getPersona());
							if (persona != null) {
								auxobj.getSysUsuario().setIdPersona(persona.getIdPer());
								auxobj.getSysUsuario().setIdSysPerfil(auxperfil.getIdPrf());
								auxobj.getSysUsuario().setUsrClave(
										Seguridad.encrypt(key, iv, auxobj.getSysUsuario().getUsrClave().trim()));
								auxobj.getSysUsuario().setUsrActivo(true);
								auxobj.getSysUsuario().setUsrFechaCreado(utilFecha);
								auxobj.getSysUsuario().setUsrFechaEditado(utilFecha);
								if (this.create(auxobj.getSysUsuario())) {
									// Mensajes.mensajeInfo(null, "Se creo correctamente el registro", null);
									return true;
								} else {
									// Mensajes.mensajeError(null, "No se registro el usuario", null);
								}
							} else {
								// Mensajes.mensajeError(null, "No se registro la persona", null);
							}
						}
					}
				}
			} else {
				// Mensajes.mensajeError(null, "No se pudo realizar la operacion", null);
				// Mensajes.mensajeWarn(null, "No existe perfil configurado", "Por favor informe
				// al administrador");
			}
		} catch (NumberFormatException e) {
			// log.error(e);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return success;
	}

	public boolean validacionCedula(String apercedula) throws Exception {
		Valida v = new Valida();
		boolean valor = false;
		try {
			if (apercedula != null) {
				valor = v.validarCedula(apercedula);
				if (valor == false) {
					// Mensajes.mensajeError(null, "Cédula: Documento es incorrecto. ", null);
				}
			}
		} catch (Exception e) {
			// log.error(e);
		}

		return valor;
	}

	@Override
	public void convertirDtoToEntity(SysUsuarioDTO objectDTO, SysUsuario objectEntity) {
		BeanUtils.copyProperties(objectDTO, objectEntity, "idUsr", "persona", "sysPerfil", "idPersona", "idSysPerfil");
		if (objectDTO.getIdPersona() != null) {
			Persona persona = this.personaRepository.findById(objectDTO.getIdPersona()).orElse(null);
			objectEntity.setPersona(persona);
		}
		if (objectDTO.getIdSysPerfil() != null) {
			SysPerfil sysPerfil = this.sysPerfilRepository.findById(objectDTO.getIdSysPerfil()).orElse(null);
			objectEntity.setSysPerfil(sysPerfil);
		}
	}

	@Override
	public SysUsuarioDTO convertirEntityToDto(SysUsuario objectEntity, boolean loadOneR, boolean loadAllList) {
		SysUsuarioDTO objectDTO = new SysUsuarioDTO();
		if (loadOneR)
			BeanUtils.copyProperties(objectEntity, objectDTO, "persona", "sysPerfil");
		else
			BeanUtils.copyProperties(objectEntity, objectDTO, "usrClave", "persona", "sysPerfil");
		if (loadOneR) {
			if (objectEntity.getPersona() != null) {
				PersonaDTO persona = this.personaService.convertirEntityToDto(objectEntity.getPersona(), false, false);
				objectDTO.setPersona(persona);
				objectDTO.setIdPersona(persona.getIdPer());
			}
			if (objectEntity.getSysPerfil() != null && objectEntity.getSysPerfil().getIdPrf() != null) {
				objectDTO.setSysPerfil(
						this.sysPerfilService.convertirEntityToDto(objectEntity.getSysPerfil(), true, false));
				objectDTO.setIdSysPerfil(objectEntity.getSysPerfil().getIdPrf());
			}
		}
		return objectDTO;
	}

	public SysUsuarioDTO convertirProjectionToDto(SysUsuarioProjection objectEntity, boolean loadOneR,
			boolean loadAllList) {
		SysUsuarioDTO objectDTO = new SysUsuarioDTO();
		objectDTO.setIdUsr(objectEntity.getIdUsr());
		objectDTO.setUsrUsuario(objectEntity.getUsrUsuario());
		objectDTO.setUsrClave(objectEntity.getUsrClave());
		objectDTO.setUsrCorreo(objectEntity.getUsrCorreo());
		objectDTO.setUsrActivo(objectEntity.getUsrActivo());
		objectDTO.setUsrFechaCreado(objectEntity.getUsrFechaCreado());
		objectDTO.setUsrFechaEditado(objectEntity.getUsrFechaEditado());
		if (loadOneR) {
			if (objectEntity.getIdPersona() != null) {
				objectDTO.setPersona(this.personaService.obtenerPersonaPorId(objectEntity.getIdPersona(), false));
				objectDTO.setIdPersona(objectEntity.getIdPersona());
			}
			if (objectEntity.getIdSysPerfil() != null) {
				objectDTO.setSysPerfil(this.sysPerfilService.obtenerPerfilPorId(objectEntity.getIdSysPerfil(), false));
				objectDTO.setIdSysPerfil(objectEntity.getIdSysPerfil());
			}
		}
		return objectDTO;
	}

}
