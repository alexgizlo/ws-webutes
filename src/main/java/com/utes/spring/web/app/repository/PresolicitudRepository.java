package com.utes.spring.web.app.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.utes.spring.web.app.entity.Presolicitud;
import com.utes.spring.web.app.projection.PresolicitudProjection;

@Repository
public interface PresolicitudRepository extends JpaRepository<Presolicitud, Integer> {

	List<Presolicitud> findByPersonaPerCedula(String cedula, Sort sort);

	List<Presolicitud> findByPersonaPerCedulaAndInscripcionIdIns(String cedula, Integer idIns, Sort sort);

	List<Presolicitud> findByPersonaIdPerAndInscripcionIdIns(Integer idPer, Integer idIns, Sort sort);

	List<Presolicitud> findByPslIdEstado(Integer pslIdEstado);

	List<Presolicitud> findByPersonaIdPerIn(List<Integer> ids);

	List<Presolicitud> findByPslIdOpcionAndPslIdEstadoIn(Integer pslIdOpcion, List<Integer> listPslIdEstado);

	List<Presolicitud> findByPslIdOpcionInAndPslIdEstadoIn(List<Integer> listPslIdOpcion,
			List<Integer> listPslIdEstado);

	@Query(nativeQuery = true, value = "SELECT id_psl FROM presolicitud WHERE fk_per =:idpersona order by 1 desc limit 1 ")
	Integer obtenerIdPresolicidutxUltimoRegistrado(@Param("idpersona") Integer idpersona);

	@Query(nativeQuery = true, value = "SELECT 	id_psl as idPsl, fk_per as idPer, fk_ins as idIns, psl_idestado as pslIdEstado, psl_idopcion as pslIdOpcion, psl_fecha as pslFecha, psl_mensaje as pslMensaje, " 
			+ "    	psl_fecharevision as pslFechaRevision, psl_observacion as pslObservacion, psl_prerevision as pslPrerevision, psl_fechaprerevision as pslFechaPrerevision, psl_idestadoanterior as pslIdEstadoAnterior, psl_activo as pslActivo " 
			+ " FROM 	presolicitud P                                    								"
			+ " 	INNER JOIN inscripcion I ON P.fk_ins = I.id_ins                               		"
			+ " WHERE	P.psl_activo = true                                                       		"
			+ " 		AND (:idinsNull OR I.id_ins = :idins)                                           "
			+ " 		AND (:idopcNull OR P.psl_idopcion = :idopcion)                                  "
			+ " 		AND (:idestadoNull OR P.psl_idestado = :idestado)                               "
			+ " 		AND (:rangoFecha OR P.psl_fecha BETWEEN :finiini AND :finifin)					")
	List<PresolicitudProjection> obtenerListadoPresolicitudConsulta(@Param("idins") Integer idInscripcion, 
			@Param("idopcion") Integer idOpcion, @Param("idestado") Integer idEstado, 
			@Param("finiini") Date finiini, @Param("finifin") Date finifin, 
			@Param("idinsNull") Boolean idinsNull, @Param("idopcNull") Boolean idopcNull, 
			@Param("idestadoNull") Boolean idestadoNull, @Param("rangoFecha") Boolean rangoFechaNull);

}
