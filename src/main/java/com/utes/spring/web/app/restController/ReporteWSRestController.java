package com.utes.spring.web.app.restController;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.utes.spring.web.app.dto.EvolucionDTO;
import com.utes.spring.web.app.dto.HitoDTO;
import com.utes.spring.web.app.dto.PresolicitudDTO;
import com.utes.spring.web.app.dto.TemaDTO;
import com.utes.spring.web.app.service.impl.ReporteService;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping(value = "/api/reporte")
public class ReporteWSRestController {

	@Autowired
	private ReporteService reporte;	
	
	@RequestMapping(value = "/reporteInscripcionEstudiante/{idprsl}", method = RequestMethod.GET)
	public void reporteInscripcionEstudiante(@PathVariable(name = "idprsl") Integer idprsl,
			@RequestHeader(value = "usuario", required = true) String usuario,
			HttpServletResponse response) throws IOException, JRException {
		JasperPrint jasperPrint = null;
		response.setContentType("application/x-download");
		response.setHeader("Content-Disposition", String.format("attachment; filename=\"rptInscripcionEstudiante.pdf\""));
		OutputStream out = response.getOutputStream();
		jasperPrint = reporte.reporteInscripcionEstudiante(idprsl, usuario);
		JasperExportManager.exportReportToPdfStream(jasperPrint, out);
	}
		
	@RequestMapping(value = "/reporteInscripcionListado", method = RequestMethod.GET)
	public void reporteInscripcionListado(@RequestBody List<PresolicitudDTO> listado,
			@RequestHeader(value = "usuario", required = true) String usuario,
			HttpServletResponse response) throws IOException, JRException {
		JasperPrint jasperPrint = null;
		response.setContentType("application/x-download");
		response.setHeader("Content-Disposition", String.format("attachment; filename=\"rptInscripcionListado.pdf\""));
		OutputStream out = response.getOutputStream();
		jasperPrint = reporte.reporteInscripcionListado(usuario, listado);
		JasperExportManager.exportReportToPdfStream(jasperPrint, out);
	}
	
	@RequestMapping(value = "/reporteHito/{idPersona}/{idTema}", method = RequestMethod.GET)
	public void reporteHito(@PathVariable(name = "idPersona") Integer idPersona,
			@PathVariable(name = "idTema") Integer idTema,
			HttpServletResponse response) throws IOException, JRException {
		JasperPrint jasperPrint = null;
		response.setContentType("application/x-download");
		response.setHeader("Content-Disposition", String.format("attachment; filename=\"rptHito.pdf\""));
		OutputStream out = response.getOutputStream();
		jasperPrint = reporte.reporteHito(idPersona, idTema);
		JasperExportManager.exportReportToPdfStream(jasperPrint, out);
	}
	
	@RequestMapping(value = "/reporteEvolucion/{idPersona}/{idTema}", method = RequestMethod.GET)
	public void reporteEvolucion(@PathVariable(name = "idPersona") Integer idPersona,
			@PathVariable(name = "idTema") Integer idTema,
			HttpServletResponse response) throws IOException, JRException {
		JasperPrint jasperPrint = null;
		response.setContentType("application/x-download");
		response.setHeader("Content-Disposition", String.format("attachment; filename=\"rptEvolucionEstudiante.pdf\""));
		OutputStream out = response.getOutputStream();
		jasperPrint = reporte.reporteEvolucion(idPersona, idTema);
		JasperExportManager.exportReportToPdfStream(jasperPrint, out);
	}
	
	@RequestMapping(value = "/reporteTema/{idPersona}/{idTema}", method = RequestMethod.GET)
	public void reporteTema(@PathVariable(name = "idPersona") Integer idPersona,
			@PathVariable(name = "idTema") Integer idTema,
			HttpServletResponse response) throws IOException, JRException {
		JasperPrint jasperPrint = null;
		response.setContentType("application/x-download");
		response.setHeader("Content-Disposition", String.format("attachment; filename=\"rptTema.pdf\""));
		OutputStream out = response.getOutputStream();
		jasperPrint = reporte.reporteTema(idPersona, idTema);
		JasperExportManager.exportReportToPdfStream(jasperPrint, out);
	}
}
