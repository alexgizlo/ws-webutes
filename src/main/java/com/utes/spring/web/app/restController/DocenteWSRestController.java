package com.utes.spring.web.app.restController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.utes.spring.web.app.dto.AsignaEstudianteDTO;
import com.utes.spring.web.app.service.DocenteService;
import com.utes.spring.web.app.util.CustomException;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping(value = "/api/docente")

public class DocenteWSRestController {

	@Autowired
	private DocenteService docenteService;

	@RequestMapping(value = "/asignacion/asignaEstudianteTema", method = RequestMethod.POST, consumes = {
			"application/json" }, produces = { "application/json" + ";charset=utf-8" })
	public @ResponseBody boolean asignaEstudianteTema(@RequestBody AsignaEstudianteDTO asignaEstudianteDTO)
			throws CustomException {
		return docenteService.asignaEstudianteTema(asignaEstudianteDTO.getEnTemaDetalleSeleccion(),
				asignaEstudianteDTO.getIdpersona(), asignaEstudianteDTO.getEnusuariosesion());

	}

	@RequestMapping(value = "/getEstadobyInscripcion/{percedula}", method = RequestMethod.GET, produces = {
			"application/json" + ";charset=utf-8" })
	public @ResponseBody String getEstadobyInscripcion(@PathVariable(name = "percedula") String percedula) {
		return docenteService.getEstadobyInscripcion(percedula);
	}

}
