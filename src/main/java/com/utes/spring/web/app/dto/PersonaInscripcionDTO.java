package com.utes.spring.web.app.dto;

import java.io.Serializable;

import lombok.Data;

@Data
public class PersonaInscripcionDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	private PersonaDTO persona;
	private InscripcionDTO inscripcion;

}
