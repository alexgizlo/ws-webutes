package com.utes.spring.web.app.projection;

import java.util.Date;


 public interface PresolicitudProjection {

	 Integer getIdPsl();

	 Integer getIdPer();

	 Integer getIdIns();

	 Integer getPslIdEstado();

	 Integer getPslIdOpcion();

	 Date getPslFecha();

	 String getPslMensaje();

	 Date getPslFechaRevision();

	 String getPslObservacion();

	 String getPslPrerevision();

	 Date getPslFechaPrerevision();

	 Integer getPslIdEstadoAnterior();

	 Boolean getPslActivo();
	 
}
